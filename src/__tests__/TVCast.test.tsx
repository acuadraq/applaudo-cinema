import React from 'react';
import { cleanup, RenderResult, waitFor, render } from '@testing-library/react';
import server from '../mocks/server';
import routesNames from '../routes/customRoutes';
import { MyContext } from '../context/context';
import { MemoryRouter, Route, Routes } from 'react-router-dom';
import TVCast from '../components/TVDetail/TVCast';

describe('TV Cast Component Tests', () => {
  let component: RenderResult;

  beforeAll(() => server.listen());
  afterEach(() => {
    cleanup();
    server.resetHandlers();
  });
  afterAll(() => server.close());

  beforeEach(() => {
    component = render(
      <MyContext.Provider value={false}>
        <MemoryRouter initialEntries={['/tv/106651/cast']}>
          <Routes>
            <Route path={routesNames.tvCast} element={<TVCast />} />
          </Routes>
        </MemoryRouter>
      </MyContext.Provider>,
    );
  });

  it('Testing if the cast of the tv renders', async () => {
    await waitFor(() => {
      expect(component.queryByLabelText(/Loading/)).toEqual(null);
    });
    component.getByText('Hellbound');
    component.getByText('Yoo Ah-in');
    component.getByText('Lee Jae-sung');
  });
});
