import React from 'react';
import { cleanup, RenderResult, waitFor, screen, render } from '@testing-library/react';
import server from '../mocks/server';
import routesNames from '../routes/customRoutes';
import '@testing-library/jest-dom';
import Movies from '../components/MovieList/Movies';
import userEvent from '@testing-library/user-event';
import { MemoryRouter, Route, Routes } from 'react-router-dom';

describe('Movies list Component Tests', () => {
  let component: RenderResult;

  beforeAll(() => server.listen());
  afterEach(() => {
    cleanup();
    server.resetHandlers();
  });
  afterAll(() => server.close());

  beforeEach(() => {
    component = render(
      <MemoryRouter initialEntries={['/movie']}>
        <Routes>
          <Route path={routesNames.movies} element={<Movies />} />
        </Routes>
      </MemoryRouter>,
    );
  });

  it('Testing if the movies list load', async () => {
    await waitFor(() => {
      expect(component.queryByLabelText(/Loading/)).toEqual(null);
    });
    component.getByText('Red Notice');
  });

  it('Testing if the filters load', async () => {
    await waitFor(() => {
      expect(component.queryByLabelText(/Loading/)).toEqual(null);
    });
    await waitFor(() => {
      expect(component.queryByLabelText(/FilterLoading/)).toEqual(null);
    });
    component.getByText('Action');
    expect(component.queryByLabelText('R')).toBeTruthy();
  });

  it('Testing if the filter certification works', async () => {
    await waitFor(() => {
      expect(component.queryByLabelText(/Loading/)).toEqual(null);
    });
    await waitFor(() => {
      expect(component.queryByLabelText(/FilterLoading/)).toEqual(null);
    });
    component.getByText('Action');
    expect(component.queryByLabelText('R')).toBeTruthy();
    userEvent.selectOptions(screen.getByLabelText('selectCertification'), 'R');
    const button = component.queryByLabelText('buttonSearch');
    if (button) userEvent.click(button);
    await waitFor(() => {
      expect(screen.queryByText('The Suicide Squad')).toBeTruthy();
    });
  });

  it('Testing if the filter genres works', async () => {
    await waitFor(() => {
      expect(component.queryByLabelText(/Loading/)).toEqual(null);
    });
    await waitFor(() => {
      expect(component.queryByLabelText(/FilterLoading/)).toEqual(null);
    });
    const genre = component.getByText('Action');
    userEvent.click(genre);
    const button = component.queryByLabelText('buttonSearch');
    if (button) userEvent.click(button);
    await waitFor(() => {
      expect(screen.queryByText('Last Night in Soho')).toBeTruthy();
    });
  });

  it('Testing if the pagination works', async () => {
    await waitFor(() => {
      expect(component.queryByLabelText(/Loading/)).toEqual(null);
    });
    await waitFor(() => {
      expect(component.queryByLabelText(/FilterLoading/)).toEqual(null);
    });
    userEvent.click(component.getByText('2'));
    await waitFor(() => {
      expect(screen.queryByText('Red Notice')).toBeTruthy();
    });
  });
});
