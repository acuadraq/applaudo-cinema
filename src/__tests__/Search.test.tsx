import React from 'react';
import { cleanup, RenderResult, waitFor, render, screen } from '@testing-library/react';
import server from '../mocks/server';
import { MemoryRouter, Routes, Route } from 'react-router-dom';
import routesNames from '../routes/customRoutes';
import { MyContext } from '../context/context';
import Search from '../components/Search/Search';
import userEvent from '@testing-library/user-event';

describe('TV Seasons Component tests', () => {
  let component: RenderResult;
  beforeAll(() => server.listen());
  afterEach(() => {
    cleanup();
    server.resetHandlers();
  });
  afterAll(() => server.close());

  beforeEach(() => {
    component = render(
      <MyContext.Provider value={false}>
        <MemoryRouter initialEntries={['/search?query=dway']}>
          <Routes>
            <Route path={routesNames.search} element={<Search />} />
          </Routes>
        </MemoryRouter>
      </MyContext.Provider>,
    );
  });

  it('Testing if the search works', async () => {
    await waitFor(() => {
      expect(screen.queryByText('Dwayne Johnson')).toBeTruthy();
    });
  });

  it('Testing if the pagination works', async () => {
    await waitFor(() => {
      expect(screen.queryByText('Dwayne Johnson')).toBeTruthy();
    });
    userEvent.click(component.getByText('2'));
    await waitFor(() => {
      expect(screen.queryByText('Dwayne Johnson')).toBeTruthy();
    });
  });
});
