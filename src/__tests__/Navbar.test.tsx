import React from 'react';
import { screen, fireEvent } from '@testing-library/dom';
import { RenderResult } from '@testing-library/react';
import renderWithRouter from '../utils/Wrapper';
import Navbar from '../components/Navbar/Navbar';

describe('Navbar Tests', () => {
  let component: RenderResult;

  beforeEach(() => {
    component = renderWithRouter(<Navbar />);
  });

  test('Check if link redirect you to movies', () => {
    const anchor = component.getByText('Movies');
    expect(anchor).toHaveAttribute('href', '/movie');
    fireEvent.click(anchor, { click: 0 });
    expect(screen.getByTestId('location-display')).toHaveTextContent('/movie');
  });

  test('Check if link redirect you to tv', () => {
    const anchor = component.getByText('TV Shows');
    expect(anchor).toHaveAttribute('href', '/tv');
    fireEvent.click(anchor, { click: 0 });
    expect(screen.getByTestId('location-display')).toHaveTextContent('/tv');
  });

  test('Check if link redirect you to login page', () => {
    const anchor = component.getByText('Log in');
    expect(anchor).toHaveAttribute('href', '/login');
    fireEvent.click(anchor, { click: 0 });
    expect(screen.getByTestId('location-display')).toHaveTextContent('/login');
  });
});
