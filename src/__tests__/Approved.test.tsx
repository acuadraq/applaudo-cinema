import React from 'react';
import { cleanup, RenderResult, waitFor, render, prettyDOM } from '@testing-library/react';
import server from '../mocks/server';
import routesNames from '../routes/customRoutes';
import { MyContext } from '../context/context';
import { MemoryRouter, Route, Routes } from 'react-router-dom';
import Approved from '../components/LogIn/Approved';
import Favorites from '../components/Account/Favorites';

describe('Person Detail Component Tests', () => {
  let component: RenderResult;

  beforeAll(() => server.listen());
  afterEach(() => {
    cleanup();
    server.resetHandlers();
  });
  afterAll(() => server.close());

  beforeEach(() => {
    component = render(
      <MyContext.Provider value={false}>
        <MemoryRouter initialEntries={['/approved']}>
          <Routes>
            <Route path={routesNames.approved} element={<Approved />} />
            <Route path={routesNames.favorites} element={<Favorites />} />
          </Routes>
        </MemoryRouter>
      </MyContext.Provider>,
    );
  });

  it('Testing the approved component', async () => {
    await waitFor(() => {
      expect(component.queryByLabelText(/Loading/)).toEqual(null);
    });
    component.getByText('Cargo');
  });
});
