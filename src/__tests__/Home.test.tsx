import React from 'react';
import { cleanup, RenderResult, waitFor } from '@testing-library/react';
import Home from '../components/Home/Home';
import server from '../mocks/server';
import routesNames from '../routes/customRoutes';
import renderWithRouter from '../utils/Wrapper';

describe('Home Component Tests', () => {
  let component: RenderResult;

  beforeAll(() => server.listen());
  afterEach(() => {
    cleanup();
    server.resetHandlers();
  });
  afterAll(() => server.close());

  beforeEach(() => {
    component = renderWithRouter(<Home />, routesNames.home);
  });

  it('Testing if the slider render the movies', async () => {
    await waitFor(() => {
      expect(component.queryByLabelText(/Loading/)).toEqual(null);
    });
    component.getByText('Venom: Let There Be Carnage');
  });

  it('Testing if the popular movies load', async () => {
    await waitFor(() => {
      expect(component.queryByLabelText(/Loading/)).toEqual(null);
    });
    component.getByText('Spider-Man: No Way Home');
  });

  it('Testing if the top rated movies load', async () => {
    await waitFor(() => {
      expect(component.queryByLabelText(/Loading/)).toEqual(null);
    });
    component.getByText('Dilwale Dulhania Le Jayenge');
  });

  it('Testing if the tv shows on air render', async () => {
    await waitFor(() => {
      expect(component.queryByLabelText(/Loading/)).toEqual(null);
    });
    component.getByText('Hawkeye');
  });
});
