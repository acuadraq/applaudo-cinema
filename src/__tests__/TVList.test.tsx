import React from 'react';
import { cleanup, RenderResult, waitFor, screen, render } from '@testing-library/react';
import server from '../mocks/server';
import routesNames from '../routes/customRoutes';
import userEvent from '@testing-library/user-event';
import Shows from '../components/ShowsList/Shows';
import '@testing-library/jest-dom';
import { MemoryRouter, Route, Routes } from 'react-router';

describe('TV List Component Tests', () => {
  let component: RenderResult;

  beforeAll(() => server.listen());
  afterEach(() => {
    cleanup();
    server.resetHandlers();
  });
  afterAll(() => server.close());

  beforeEach(() => {
    component = render(
      <MemoryRouter initialEntries={['/tv']}>
        <Routes>
          <Route path={routesNames.shows} element={<Shows />} />
        </Routes>
      </MemoryRouter>,
    );
  });

  it('Testing if the shows list load', async () => {
    await waitFor(() => {
      expect(component.queryByLabelText(/Loading/)).toEqual(null);
    });
    component.getByText('Chucky');
  });

  it('Testing if the filters load', async () => {
    await waitFor(() => {
      expect(component.queryByLabelText(/Loading/)).toEqual(null);
    });
    await waitFor(() => {
      expect(component.queryByLabelText(/FilterLoading/)).toEqual(null);
    });
    component.getByText('Animation');
  });

  it('Testing if the filter genres works', async () => {
    await waitFor(() => {
      expect(component.queryByLabelText(/Loading/)).toEqual(null);
    });
    await waitFor(() => {
      expect(component.queryByLabelText(/FilterLoading/)).toEqual(null);
    });
    const genre = component.getByText('Animation');
    userEvent.click(genre);
    const button = component.queryByLabelText('buttonSearch');
    if (button) userEvent.click(button);
    await waitFor(() => {
      expect(screen.queryByText('Arcane')).toBeTruthy();
    });
  });

  it('Testing if the pagination works', async () => {
    await waitFor(() => {
      expect(component.queryByLabelText(/Loading/)).toEqual(null);
    });
    await waitFor(() => {
      expect(component.queryByLabelText(/FilterLoading/)).toEqual(null);
    });
    userEvent.click(component.getByText('2'));
    await waitFor(() => {
      expect(screen.queryByText('Hellbound')).toBeTruthy();
    });
  });
});
