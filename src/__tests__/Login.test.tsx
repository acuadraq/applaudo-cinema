import React from 'react';
import { cleanup, RenderResult, waitFor, render } from '@testing-library/react';
import server from '../mocks/server';
import routesNames from '../routes/customRoutes';
import { MemoryRouter, Route, Routes } from 'react-router-dom';
import Home from '../components/Home/Home';
import LogIn from '../components/LogIn/LogIn';
import userEvent from '@testing-library/user-event';

describe('Person Detail Component Tests', () => {
  let component: RenderResult;

  beforeAll(() => server.listen());
  afterEach(() => {
    cleanup();
    server.resetHandlers();
  });
  afterAll(() => server.close());

  beforeEach(() => {
    component = render(
      <MemoryRouter initialEntries={['/login']}>
        <Routes>
          <Route path={routesNames.login} element={<LogIn />} />
        </Routes>
      </MemoryRouter>,
    );
  });

  it('Testing the login component', async () => {
    userEvent.click(component.getByText('Log In'));
  });
});
