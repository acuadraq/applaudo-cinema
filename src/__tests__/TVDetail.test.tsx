import React from 'react';
import { cleanup, RenderResult, waitFor, render, act } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import server from '../mocks/server';
import { MemoryRouter, Routes, Route } from 'react-router-dom';
import routesNames from '../routes/customRoutes';
import TvDetail from '../components/TVDetail/TvDetail';
import { MyContext } from '../context/context';

describe('TV Detail Component tests', () => {
  let component: RenderResult;
  beforeAll(() => server.listen());
  afterEach(() => {
    cleanup();
    server.resetHandlers();
  });
  afterAll(() => server.close());

  const contextValue = {
    id: 11471181,
    name: 'Alex Cuadra',
    sessionId: '491ab3e9da6687f6b99a76cd165e21aee2e650d3',
    username: 'AlexC',
    avatar: '/5S9dcNdSWzwcrrTrhR90k6rW45W.jpg',
  };

  beforeEach(() => {
    act(() => {
      component = render(
        <MyContext.Provider value={contextValue}>
          <MemoryRouter initialEntries={['/tv/88329']}>
            <Routes>
              <Route path={routesNames.showDetail} element={<TvDetail />} />
            </Routes>
          </MemoryRouter>
        </MyContext.Provider>,
      );
    });
  });

  it('Testing if the tv renders', async () => {
    await waitFor(() => {
      expect(component.queryByLabelText(/Loading/)).toEqual(null);
    });
    component.getByText('Hawkeye');
  });

  it('Testing if the tv cast renders', async () => {
    await waitFor(() => {
      expect(component.queryByLabelText(/Loading/)).toEqual(null);
    });
    component.getByText('Jeremy Renner');
  });
  it('Testing if the tv season renders', async () => {
    await waitFor(() => {
      expect(component.queryByLabelText(/Loading/)).toEqual(null);
    });
    component.queryByLabelText(/Last Seasson/i);
  });

  it('Testing if the tv images renders', async () => {
    await waitFor(() => {
      expect(component.queryByLabelText(/Loading/)).toEqual(null);
    });
    expect(component.queryByLabelText('img-1')).toBeTruthy();
  });

  it('Testing if the tv similar renders', async () => {
    await waitFor(() => {
      expect(component.queryByLabelText(/Loading/)).toEqual(null);
    });
    component.getByText('The Walking Dead');
  });

  it('Testing if the add and remove favorites function works', async () => {
    await waitFor(() => {
      expect(component.queryByLabelText(/Loading/)).toEqual(null);
    });
    const button = component.getByText('Add To Favorites');
    userEvent.click(button);
    await waitFor(() => {
      expect(component.getByText('Remove From Favorites')).toBeTruthy();
    });
    const button2 = component.getByText('Remove From Favorites');
    userEvent.click(button2);
    const button3 = component.getByText('Delete');
    userEvent.click(button3);
    await waitFor(() => {
      expect(component.getByText('Add To Favorites')).toBeTruthy();
    });
  });
});
