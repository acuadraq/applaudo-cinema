import React from 'react';
import { cleanup, RenderResult, waitFor, render, prettyDOM } from '@testing-library/react';
import server from '../mocks/server';
import routesNames from '../routes/customRoutes';
import { MemoryRouter, Route, Routes } from 'react-router-dom';
import Home from '../components/Home/Home';
import NotFound from '../components/NotFound/NotFound';

describe('Person Detail Component Tests', () => {
  let component: RenderResult;

  beforeAll(() => server.listen());
  afterEach(() => {
    cleanup();
    server.resetHandlers();
  });
  afterAll(() => server.close());

  beforeEach(() => {
    component = render(
      <MemoryRouter initialEntries={['/asd']}>
        <Routes>
          <Route path="*" element={<NotFound />} />
          <Route path={routesNames.home} element={<Home />} />
        </Routes>
      </MemoryRouter>,
    );
  });

  it('Testing the not found component', async () => {
    component.getByText('Go home');
  });
});
