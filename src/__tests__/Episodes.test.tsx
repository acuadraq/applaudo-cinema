import React from 'react';
import { cleanup, RenderResult, waitFor, render, act, screen } from '@testing-library/react';
import server from '../mocks/server';
import { MemoryRouter, Routes, Route } from 'react-router-dom';
import routesNames from '../routes/customRoutes';
import { MyContext } from '../context/context';
import Episodes from '../components/TVDetail/Episodes';
import userEvent from '@testing-library/user-event';

describe('TV Seasons Component tests', () => {
  let component: RenderResult;
  beforeAll(() => server.listen());
  afterEach(() => {
    cleanup();
    server.resetHandlers();
  });
  afterAll(() => server.close());

  beforeEach(() => {
    act(() => {
      component = render(
        <MyContext.Provider value={false}>
          <MemoryRouter initialEntries={['/tv/1416/seasons/7']}>
            <Routes>
              <Route path={routesNames.episodes} element={<Episodes />} />
            </Routes>
          </MemoryRouter>
        </MyContext.Provider>,
      );
    });
  });

  it('Testing if the name of the season renders', async () => {
    await waitFor(() => {
      expect(component.queryByLabelText(/Loading/)).toEqual(null);
    });
    component.getByText('Season 7');
  });

  it('Testing if the episodes are rendering', async () => {
    await waitFor(() => {
      expect(component.queryByLabelText(/Loading/)).toEqual(null);
    });
    component.getByText(`1. With You I'm Born Again`);
  });

  it('Testing the change page', async () => {
    await waitFor(() => {
      expect(component.queryByLabelText(/Loading/)).toEqual(null);
    });
    userEvent.click(component.getByText('2'));
    await waitFor(() => {
      expect(screen.queryByText('11. Disarm')).toBeTruthy();
    });
  });
});
