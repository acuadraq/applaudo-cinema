import React from 'react';
import { cleanup, RenderResult, waitFor, render } from '@testing-library/react';
import server from '../mocks/server';
import routesNames from '../routes/customRoutes';
import { MyContext } from '../context/context';
import { MemoryRouter, Route, Routes } from 'react-router-dom';
import Favorites from '../components/Account/Favorites';
import userEvent from '@testing-library/user-event';

describe('Account Favorite Movies Detail Component Tests', () => {
  let component: RenderResult;

  beforeAll(() => server.listen());
  afterEach(() => {
    cleanup();
    server.resetHandlers();
  });
  afterAll(() => server.close());

  const contextValue = {
    id: 11471181,
    name: 'Alex Cuadra',
    sessionId: '491ab3e9da6687f6b99a76cd165e21aee2e650d3',
    username: 'AlexC',
    avatar: '/5S9dcNdSWzwcrrTrhR90k6rW45W.jpg',
  };

  beforeEach(() => {
    component = render(
      <MyContext.Provider value={contextValue}>
        <MemoryRouter initialEntries={['/favorites']}>
          <Routes>
            <Route path={routesNames.favorites} element={<Favorites />} />
          </Routes>
        </MemoryRouter>
      </MyContext.Provider>,
    );
  });

  it('Testing if the username info loads', async () => {
    await waitFor(() => {
      expect(component.queryByLabelText(/Loading/)).toEqual(null);
    });
    component.getByText('Alex Cuadra');
  });

  it('Testing if the movies are rendering', async () => {
    await waitFor(() => {
      expect(component.queryByLabelText(/Loading/)).toEqual(null);
    });
    component.getByText('Cargo');
  });

  it('Testing the remove from favorites button', async () => {
    await waitFor(() => {
      expect(component.queryByLabelText(/Loading/)).toEqual(null);
    });
    component.getByText('Cargo');
    const button = component.queryByLabelText('removeFavorite-425972');
    if (button) userEvent.click(button);
    userEvent.click(component.getByText('Delete'));
  });
});
