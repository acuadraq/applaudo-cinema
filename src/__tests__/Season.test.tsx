import React from 'react';
import { cleanup, RenderResult, waitFor, render, act } from '@testing-library/react';
import server from '../mocks/server';
import { MemoryRouter, Routes, Route } from 'react-router-dom';
import routesNames from '../routes/customRoutes';
import { MyContext } from '../context/context';
import Seasons from '../components/TVDetail/Seasons';

describe('TV Seasons Component tests', () => {
  let component: RenderResult;
  beforeAll(() => server.listen());
  afterEach(() => {
    cleanup();
    server.resetHandlers();
  });
  afterAll(() => server.close());

  beforeEach(() => {
    act(() => {
      component = render(
        <MyContext.Provider value={false}>
          <MemoryRouter initialEntries={['/tv/88329/seasons']}>
            <Routes>
              <Route path={routesNames.seasons} element={<Seasons />} />
            </Routes>
          </MemoryRouter>
        </MyContext.Provider>,
      );
    });
  });

  it('Testing if the name of the tv renders', async () => {
    await waitFor(() => {
      expect(component.queryByLabelText(/Loading/)).toEqual(null);
    });
    component.getByText('Hawkeye');
  });

  it('Testing if the season of the tv renders', async () => {
    await waitFor(() => {
      expect(component.queryByLabelText(/Loading/)).toEqual(null);
    });
    component.getByText('Season 1');
  });
});
