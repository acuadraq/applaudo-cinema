import React from 'react';
import { cleanup, RenderResult, waitFor, render, screen } from '@testing-library/react';
import server from '../mocks/server';
import routesNames from '../routes/customRoutes';
import { MyContext } from '../context/context';
import { MemoryRouter, Route, Routes } from 'react-router-dom';
import MovieCast from '../components/MovieDetail/MovieCast';
import userEvent from '@testing-library/user-event';

describe('Movie Cast Component Tests', () => {
  let component: RenderResult;

  beforeAll(() => server.listen());
  afterEach(() => {
    cleanup();
    server.resetHandlers();
  });
  afterAll(() => server.close());

  beforeEach(() => {
    component = render(
      <MyContext.Provider value={false}>
        <MemoryRouter initialEntries={['/movie/568124/cast']}>
          <Routes>
            <Route path={routesNames.movieCast} element={<MovieCast />} />
          </Routes>
        </MemoryRouter>
      </MyContext.Provider>,
    );
  });

  it('Testing if the cast of the tv renders', async () => {
    await waitFor(() => {
      expect(component.queryByLabelText(/Loading/)).toEqual(null);
    });
    component.getByText('Encantada');
    component.getByText('Stephanie Beatriz');
    component.getByText('Mehrdad Isvandi');
  });

  it('Testing if the cast pagination from the tv works', async () => {
    await waitFor(() => {
      expect(component.queryByLabelText(/Loading/)).toEqual(null);
    });
    userEvent.click(component.getByText('2'));
    await waitFor(() => {
      expect(screen.queryByText('Maluma')).toBeTruthy();
    });
  });
});
