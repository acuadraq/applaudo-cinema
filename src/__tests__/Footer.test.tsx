import React from 'react';
import { screen } from '@testing-library/dom';
import { RenderResult } from '@testing-library/react';
import renderWithRouter from '../utils/Wrapper';
import Footer from '../components/Footer/Footer';
import userEvent from '@testing-library/user-event';

describe('Footer Tests', () => {
  let component: RenderResult;

  beforeEach(() => {
    component = renderWithRouter(<Footer />);
  });

  test('Check if link redirect you to movies', () => {
    const anchor = component.getByText('Movies');
    userEvent.click(anchor);
    expect(screen.getByTestId('location-display')).toHaveTextContent('/movie');
  });

  test('Check if link redirect you to tv', () => {
    const anchor = component.getByText('TV Shows');
    userEvent.click(anchor);
    expect(screen.getByTestId('location-display')).toHaveTextContent('/tv');
  });

  test('Check if link redirect you to login page', () => {
    const anchor = component.getByText('Log In');
    userEvent.click(anchor);
    expect(screen.getByTestId('location-display')).toHaveTextContent('/login');
  });
});
