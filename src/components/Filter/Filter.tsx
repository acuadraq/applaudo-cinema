import React, { useEffect, useState } from 'react';
import { getGenresMovie, getCertificationsMovies } from '../../adapters/MoviesFetch';
import { useNavigate } from 'react-router-dom';
import { getGenreShows } from '../../adapters/ShowFetch';
import { IGenres } from '../../interfaces/genres';
import { ICertification, IOnlyCertification } from '../../interfaces/certification';

type filterProps = {
  getInfo: (page?: number, queries?: string) => void;
  changeQuery: (newQuery: string) => void;
  activateError: () => void;
  type: string;
};

const Filter = ({ getInfo, changeQuery, activateError, type }: filterProps) => {
  const [genres, setGenres] = useState<IGenres>();
  const [certifications, setCertifications] = useState<IOnlyCertification[]>();
  const [loading, setLoading] = useState<boolean>(true);
  const [genresSelected, setGenresSelected] = useState<number[]>([]);
  const [dateFrom, setDateFrom] = useState<string>('');
  const [dateTo, setDateTo] = useState<string>('');
  const [select, setSelect] = useState<string>();
  const navigate = useNavigate();
  const [previousPath, setPreviousPath] = useState<string>('');
  const urlSearch = new URL(window.location.href);

  const selectGenre = (id: number) => {
    if (genresSelected.includes(id)) {
      const genresCopy: number[] = [...genresSelected];
      genresCopy.splice(genresCopy.indexOf(id), 1);
      setGenresSelected(genresCopy);
      return;
    }
    setGenresSelected([...genresSelected, id]);
  };

  const getCertifications = async () => {
    try {
      const list: ICertification = await getCertificationsMovies();
      setCertifications(list.certifications.US);
    } catch (err) {
      activateError();
    }
  };

  const getGenres = async () => {
    try {
      let list: IGenres;
      if (type === 'movie') {
        list = await getGenresMovie();
      } else {
        list = await getGenreShows();
      }
      setGenres(list);
      setLoading(false);
      if (type === 'movie') getCertifications();
    } catch (err) {
      activateError();
    }
  };

  const submitFilter = () => {
    if (select) {
      urlSearch.searchParams.set('certification_country', 'US');
      urlSearch.searchParams.set('certification', select);
    }
    urlSearch.searchParams.set('with_genres', genresSelected.join(','));
    urlSearch.searchParams.set(
      `${type === 'movie' ? 'release_date.gte' : 'air_date.gte'}`,
      dateFrom,
    );
    urlSearch.searchParams.set(`${type === 'movie' ? 'release_date.lte' : 'air_date.lte'}`, dateTo);
    if (previousPath != urlSearch.search) {
      navigate(`${urlSearch.search}`);
      getInfo(1, urlSearch.search.replace('?', '&'));
      changeQuery(urlSearch.search.replace('?', '&'));
      setPreviousPath(urlSearch.search);
    }
  };

  useEffect(() => {
    getGenres();
  }, []);

  return (
    <>
      <div className="w-full bg-cardscolor rounded-lg shadow-lg p-3">
        <h2 className="text-palidcolor text-xl mb-2">Filters</h2>
        <hr />
        {loading && <div className="animation__loading" aria-label="FilterLoading" />}
        {!loading && (
          <>
            {type === 'movie' && (
              <>
                <div className="my-5">
                  <h3 className="text-sm text-hovertext">Certification</h3>
                  <label>
                    <select
                      name="certification"
                      id="certification"
                      value={select}
                      aria-label="selectCertification"
                      onChange={e => setSelect(e.target.value)}
                      className="w-full mt-3 rounded-lg px-2 py-1 bg-bgsecondcolor text-palidcolor outline-none"
                    >
                      <option value=" ">All</option>
                      {certifications?.map(certification => (
                        <option
                          key={certification.order}
                          aria-label={certification.certification}
                          value={certification.certification}
                        >
                          {certification.certification}
                        </option>
                      ))}
                    </select>
                  </label>
                </div>
                <hr />
              </>
            )}
            <div className="my-5">
              <h3 className="text-sm text-hovertext">Genres</h3>
              <div className="flex flex-wrap mt-3">
                {genres?.genres.map(genre => (
                  <span
                    key={genre.id}
                    onClick={() => selectGenre(genre.id)}
                    className={`${
                      genresSelected.includes(genre.id) ? 'bg-bgsecondcolor' : ''
                    } m-1 text-palidcolor text-xs rounded-full ring-2 ring-bgsecondcolor p-2 cursor-pointer hover:bg-bgsecondcolor`}
                  >
                    {genre.name}
                  </span>
                ))}
              </div>
            </div>
            <hr />
          </>
        )}
        <div className="my-5">
          <h3 className="text-sm text-hovertext">Release Dates</h3>
          <div className="mt-3 text-palidcolor">
            <label htmlFor="from">From</label>
            <input
              type="date"
              name="from"
              id="from"
              value={dateFrom}
              onChange={e => setDateFrom(e.target.value)}
              className="w-full px-2 py-1 mt-2 mb-4 bg-bgsecondcolor rounded-lg outline-none text-sm"
            />
            <label htmlFor="to">To</label>
            <input
              type="date"
              name="to"
              id="to"
              value={dateTo}
              onChange={e => setDateTo(e.target.value)}
              className="w-full px-2 py-1 mt-2 bg-bgsecondcolor rounded-lg outline-none text-sm"
            />
          </div>
        </div>
      </div>
      <div className="mt-5 grid items-center px-16">
        <button
          onClick={submitFilter}
          aria-label="buttonSearch"
          className="bg-cardscolor px-3 py-2 font-bold uppercase rounded-lg text-palidcolor shadow-md hover:shadow-xl"
        >
          Search
        </button>
      </div>
    </>
  );
};

export default Filter;
