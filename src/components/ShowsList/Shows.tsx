import React, { useEffect, useState } from 'react';
import { ITvShows } from '../../interfaces/tvshows';
import { getShows } from '../../adapters/ShowFetch';
import Error from '../Error/Error';
import { getPosterImage } from '../../adapters/Image';
import Card from '../Card/Card';
import ReactPaginate from 'react-paginate';
import Filter from '../Filter/Filter';

const Shows = () => {
  const [shows, setShows] = useState<ITvShows>();
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<boolean>(false);
  const [query, setQuery] = useState<string>('');

  const getTVShows = async (page?: number, queries?: string) => {
    setLoading(true);
    try {
      const list: ITvShows = await getShows(page, queries);
      setShows(list);
      setLoading(false);
    } catch (err) {
      setError(true);
      setLoading(false);
    }
  };

  const changePage = (selected: number) => {
    getTVShows(selected + 1, query);
    window.scrollTo(0, 0);
  };

  const changeQuery = (newQuery: string) => setQuery(newQuery);

  const activateError = () => setError(true);

  useEffect(() => {
    getTVShows();
  }, []);
  return (
    <>
      {error && <Error />}
      {!shows && loading && <div className="animation__loading mt-5" aria-label="Loading" />}
      {shows && !error && (
        <div className="xl:container px-4 sm:px-8 lg:px-16 xl:px-24 mx-auto py-5 md:py-10">
          <h1 className="section__title">Popular TV Shows</h1>
          <div className="grid grid-cols-1 md:grid-cols-3 xl:grid-cols-4 gap-5 mt-5">
            <div>
              <Filter
                getInfo={getTVShows}
                changeQuery={changeQuery}
                activateError={activateError}
                type="tv"
              />
            </div>
            <div className="md:col-span-2 xl:col-span-3">
              {loading && <div className="animation__loading" />}
              {!loading && (
                <div className="flex flex-wrap justify-around md:grid md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-5">
                  {shows?.results.length === 0 && (
                    <p className="text-palidcolor text-lg">No TV Shows were found</p>
                  )}
                  {shows?.results.map(show => (
                    <div key={show.id} className="transform hover:-translate-y-2">
                      <Card
                        id={show.id}
                        title={show.name}
                        imgUrl={show.poster_path ? getPosterImage(show.poster_path) : null}
                        type="/tv"
                      />
                    </div>
                  ))}
                </div>
              )}

              <ReactPaginate
                previousLabel={'<'}
                nextLabel={'>'}
                pageCount={shows.total_pages}
                pageRangeDisplayed={2}
                marginPagesDisplayed={1}
                onPageChange={e => changePage(e.selected)}
                containerClassName={'container__pagination'}
                pageClassName={'md:mx-5'}
                pageLinkClassName={'page__link'}
                activeLinkClassName={'bg-cardscolor font-bold'}
                previousLinkClassName={'previous__after'}
                nextLinkClassName={'previous__after'}
                disabledClassName={'prev__after--disabled'}
              />
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default Shows;
