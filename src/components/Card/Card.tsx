import React from 'react';
import { LazyLoadImage } from 'react-lazy-load-image-component';
import 'react-lazy-load-image-component/src/effects/blur.css';
import { Link } from 'react-router-dom';

type cardProps = {
  id: number;
  title: string | undefined;
  imgUrl: string | null;
  type: string;
  searchType?: string;
};

const Card = ({ id, title, imgUrl, type, searchType }: cardProps) => {
  return (
    <div className="grid grid-cols-1">
      <div className="mb-3">
        <Link to={`${type}/${id}`} className="mb-3 transition-transform ease-in duration-100">
          <LazyLoadImage
            className="h-cardsheight w-cardswidth"
            src={`${imgUrl ? imgUrl : 'https://i.imgur.com/4XQUcDj.png'}`}
            alt={title}
            effect="blur"
          />
        </Link>
      </div>
      <div>
        <h3 className="text-palidcolor font-bold">
          {title && (
            <Link to={`${type}/${id}`}>
              {title.length > 25 ? `${title.slice(0, 22)}...` : title}
            </Link>
          )}
        </h3>
        {searchType && (
          <h4 className="text-xs break-all uppercase tracking-widest text-palidcolor">
            {searchType.length > 30 ? `${searchType.slice(0, 28)}...` : searchType}
          </h4>
        )}
      </div>
    </div>
  );
};

export default Card;
