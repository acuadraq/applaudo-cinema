import React, { useState, useEffect, useContext } from 'react';
import { useParams } from 'react-router-dom';
import {
  checkTVInFavorites,
  getSimilarTV,
  getTvCast,
  getTvDetail,
  getTvImages,
  getTvReviews,
} from '../../adapters/ShowFetch';
import { MyContext } from '../../context/context';
import { ICast } from '../../interfaces/credits';
import { IImages } from '../../interfaces/images';
import { IIsInFavorites, IStoredAccount } from '../../interfaces/login';
import { IReview } from '../../interfaces/reviews';
import { IShow } from '../../interfaces/tv';
import { ITvShows } from '../../interfaces/tvshows';
import DetailHeader from '../Detail/DetailHeader';
import DetailInfo from '../Detail/DetailInfo';
import Error from '../Error/Error';

const TvDetail = () => {
  const [tv, setTv] = useState<IShow>();
  const [cast, setCast] = useState<ICast>();
  const [isInFavorite, setIsInFavorite] = useState<boolean | string>('NOT');
  const [images, setImages] = useState<IImages>();
  const [similarTVS, setSimilarTVS] = useState<ITvShows>();
  const [reviews, setReviews] = useState<IReview>();
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<boolean>(false);
  const { id } = useParams<string>();
  const isLogged = useContext(MyContext);

  const getFavorites = async () => {
    const { sessionId } = isLogged as IStoredAccount;
    if (id) {
      try {
        const check: IIsInFavorites = await checkTVInFavorites(parseInt(id, 10), sessionId);
        if (check.favorite) {
          setIsInFavorite(true);
          setLoading(false);
        } else {
          setIsInFavorite(false);
          setLoading(false);
        }
      } catch (err) {
        setError(true);
        setLoading(false);
      }
    }
  };

  const getReviews = async () => {
    if (id) {
      try {
        const info: IReview = await getTvReviews(parseInt(id, 10));
        setReviews(info);
        if (isLogged) {
          getFavorites();
        } else {
          setLoading(false);
        }
      } catch (err) {
        setLoading(false);
      }
    }
  };

  const getSimilars = async () => {
    if (id) {
      try {
        const info: ITvShows = await getSimilarTV(parseInt(id, 10));
        setSimilarTVS(info);
        getReviews();
      } catch (err) {
        getReviews();
      }
    }
  };

  const getImages = async () => {
    if (id) {
      try {
        const info: IImages = await getTvImages(parseInt(id, 10));
        setImages(info);
        getSimilars();
      } catch (err) {
        getSimilars();
      }
    }
  };

  const getCast = async () => {
    if (id) {
      try {
        const info: ICast = await getTvCast(parseInt(id, 10));
        setCast(info);
        getImages();
      } catch (err) {
        setError(true);
        setLoading(false);
      }
    }
  };

  const getTvInfo = async () => {
    if (id) {
      setLoading(true);
      try {
        const info: IShow = await getTvDetail(parseInt(id, 10));
        setTv(info);
        getCast();
      } catch (err) {
        setError(true);
        setLoading(false);
      }
    }
  };

  useEffect(() => {
    getTvInfo();
  }, [id]);

  return (
    <>
      {loading && <div className="animation__loading mt-10" aria-label="Loading" />}
      {error && <Error />}
      {tv && !loading && !error && (
        <>
          <DetailHeader tv={tv} favorite={isInFavorite} type="tv" getFavorites={getFavorites} />
          <div className="big__container">
            <DetailInfo
              type="tv show"
              crew={cast}
              images={images}
              similarTvs={similarTVS}
              reviews={reviews}
              tvinfo={tv}
            />
          </div>
        </>
      )}
    </>
  );
};

export default TvDetail;
