import React, { useEffect, useState } from 'react';
import { LazyLoadImage } from 'react-lazy-load-image-component';
import 'react-lazy-load-image-component/src/effects/blur.css';
import ReactPaginate from 'react-paginate';
import { Link, useParams } from 'react-router-dom';
import { getSeasonURL } from '../../adapters/Image';
import { getTvDetail } from '../../adapters/ShowFetch';
import { IShow } from '../../interfaces/tv';
import Error from '../Error/Error';
import Header from '../Detail/Header';

const Seasons = () => {
  const [tv, setTv] = useState<IShow>();
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<boolean>(false);
  const [pageNumber, setPageNumber] = useState<number>(0);

  const seasonsPerPage = 10;
  const pagesVisited = pageNumber * seasonsPerPage;
  const { id } = useParams<string>();
  let pageCount: number = 0;

  const currentSeasons = tv?.seasons.slice(pagesVisited, pagesVisited + seasonsPerPage);

  const getTvInfo = async () => {
    if (id) {
      setLoading(true);
      try {
        const info: IShow = await getTvDetail(parseInt(id, 10));
        setTv(info);
        setLoading(false);
      } catch (err) {
        setError(true);
        setLoading(false);
      }
    }
  };

  if (tv) {
    pageCount = Math.ceil(tv.seasons.length / seasonsPerPage);
  }

  const changePage = (selected: number) => {
    setPageNumber(selected);
    window.scrollTo(0, 0);
  };

  useEffect(() => {
    getTvInfo();
  }, []);
  return (
    <>
      {loading && <div className="animation__loading mt-10" aria-label="Loading" />}
      {error && <Error />}
      {tv && (
        <div>
          <Header name={tv.name} imgUrl={tv.poster_path} link={`/tv/${tv.id}`} />
          <section className="text-palidcolor mb-10">
            {currentSeasons?.map(season => (
              <div key={season.id} className="w-full">
                <div className="big__container">
                  <div className="grid sm:grid-cols-4 xl:grid-cols-6 ">
                    <div className="mx-auto sm:mx-0">
                      <Link to={`${season.season_number}`}>
                        <LazyLoadImage
                          src={
                            season.poster_path
                              ? getSeasonURL(season.poster_path)
                              : 'https://i.imgur.com/Nd0rpzx.png'
                          }
                          alt={season.name}
                          className="h-seasonheight w-seasonwidth rounded-md hover:shadow-xl"
                          effect="blur"
                          height="195"
                          width="135"
                        />
                      </Link>
                    </div>
                    <div className="sm:col-span-3 xl:col-span-5">
                      <Link
                        to={`${season.season_number}`}
                        className="text-2xl mb-2 font-bold hover:text-hovertext"
                      >
                        {season.name}
                      </Link>
                      <h3>
                        {season.air_date ? season.air_date.slice(0, 4) : '-'} |{' '}
                        {season.episode_count} EP
                      </h3>
                      <p className="mt-3">
                        {season.overview
                          ? season.overview
                          : `Season ${season.season_number} of ${tv.name}`}
                      </p>
                    </div>
                  </div>
                </div>
                <hr className="bg-gray-500 h-0.5 border-none" />
              </div>
            ))}
            {tv.seasons.length > 10 && (
              <ReactPaginate
                previousLabel={'<'}
                nextLabel={'>'}
                pageCount={pageCount}
                pageRangeDisplayed={2}
                marginPagesDisplayed={1}
                onPageChange={e => changePage(e.selected)}
                containerClassName={'container__pagination'}
                pageClassName={'md:mx-5'}
                pageLinkClassName={'page__link'}
                activeLinkClassName={'bg-cardscolor font-bold'}
                previousLinkClassName={'previous__after'}
                nextLinkClassName={'previous__after'}
                disabledClassName={'prev__after--disabled'}
              />
            )}
          </section>
        </div>
      )}
    </>
  );
};

export default Seasons;
