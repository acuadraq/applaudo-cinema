import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { LazyLoadImage } from 'react-lazy-load-image-component';
import 'react-lazy-load-image-component/src/effects/blur.css';
import ReactPaginate from 'react-paginate';
import Error from '../Error/Error';
import { ISeason } from '../../interfaces/season';
import { getSeason } from '../../adapters/ShowFetch';
import { getPosterImage } from '../../adapters/Image';
import Header from '../Detail/Header';

const Episodes = () => {
  const [season, setSeason] = useState<ISeason>();
  const { id, seasonNumber } = useParams<string>();
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<boolean>(false);

  const [pageNumber, setPageNumber] = useState<number>(0);

  const episodesPerPage = 10;
  const pagesVisited = pageNumber * episodesPerPage;
  let pageCount: number = 0;

  const currentEpisodes = season?.episodes.slice(pagesVisited, pagesVisited + episodesPerPage);

  const getSeasonDetail = async () => {
    if (id && seasonNumber) {
      setLoading(true);
      try {
        const info: ISeason = await getSeason(parseInt(id, 10), parseInt(seasonNumber, 10));
        setSeason(info);
        setLoading(false);
      } catch (err) {
        setError(true);
        setLoading(false);
      }
    }
  };

  if (season) {
    pageCount = Math.ceil(season.episodes.length / episodesPerPage);
  }

  const changePage = (selected: number) => {
    setPageNumber(selected);
    window.scrollTo(0, 0);
  };

  useEffect(() => {
    getSeasonDetail();
  }, []);

  return (
    <>
      {loading && <div className="animation__loading mt-10" aria-label="Loading" />}
      {error && <Error />}
      {season && (
        <div>
          <Header name={season.name} imgUrl={season.poster_path} link={`/tv/${id}/seasons`} />

          <section className="text-palidcolor big__container">
            <h2 className="text-palidcolor text-lg mb-5">Episodes {season.episodes.length}</h2>
            {currentEpisodes?.length === 0 && (
              <p className="text-palidcolor text-xl">There are not episodes to this season</p>
            )}
            {currentEpisodes?.map(episode => (
              <div
                key={episode.id}
                className="grid md:grid-cols-3 2xl:grid-cols-4 mb-10 rounded-lg gap-10"
              >
                <div className="grid items-center">
                  <div className="flex items-center mx-auto">
                    <LazyLoadImage
                      src={
                        episode.still_path
                          ? getPosterImage(episode.still_path)
                          : 'https://i.imgur.com/gYDs9Hv.png'
                      }
                      alt={episode.name}
                      effect="blur"
                      className="w-episodewidth"
                      width="300"
                      height="169"
                    />
                  </div>
                </div>
                <div className="md:col-span-2 2xl:col-span-3 p-4 bg-cardscolor">
                  <div className="flex justify-between">
                    <h2 className="text-lg font-bold">
                      {episode.episode_number}. {episode.name}
                    </h2>
                    <div className="flex items-center">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        className="h-6 w-6"
                        viewBox="0 0 20 20"
                        fill="currentColor"
                      >
                        <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z" />
                      </svg>
                      <span>{episode.vote_average.toFixed(2)}</span>
                    </div>
                  </div>
                  <span className="text-xs">{episode.air_date.replaceAll('-', '/')}</span>
                  <p className="mt-1 text-sm">
                    {episode.overview
                      ? episode.overview
                      : `Episode ${episode.episode_number} of the ${season.season_number} season`}
                  </p>
                </div>
              </div>
            ))}
            {season.episodes.length > 10 && (
              <ReactPaginate
                previousLabel={'<'}
                nextLabel={'>'}
                pageCount={pageCount}
                pageRangeDisplayed={2}
                marginPagesDisplayed={1}
                onPageChange={e => changePage(e.selected)}
                containerClassName={'container__pagination'}
                pageClassName={'md:mx-5'}
                pageLinkClassName={'page__link'}
                activeLinkClassName={'bg-cardscolor font-bold'}
                previousLinkClassName={'previous__after'}
                nextLinkClassName={'previous__after'}
                disabledClassName={'prev__after--disabled'}
              />
            )}
          </section>
        </div>
      )}
    </>
  );
};

export default Episodes;
