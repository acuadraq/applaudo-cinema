import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import Error from '../Error/Error';
import { getTvCast, getTvDetail } from '../../adapters/ShowFetch';
import { ICast } from '../../interfaces/credits';
import { IShow } from '../../interfaces/tv';
import Header from '../Detail/Header';
import Cast from '../Detail/Cast';

const TVCast = () => {
  const [cast, setCast] = useState<ICast>();
  const [tv, setTv] = useState<IShow>();
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<boolean>(false);
  const { id } = useParams<string>();

  const getCast = async () => {
    if (id) {
      try {
        const list: ICast = await getTvCast(parseInt(id, 10));
        setCast(list);
        setLoading(false);
      } catch (err) {
        setError(true);
        setLoading(false);
      }
    }
  };

  const getDetails = async () => {
    if (id) {
      try {
        const info: IShow = await getTvDetail(parseInt(id, 10));
        setTv(info);
        getCast();
      } catch (err) {
        setError(true);
        setLoading(false);
      }
    }
  };

  useEffect(() => {
    getDetails();
  }, []);

  return (
    <>
      {loading && <div className="animation__loading mt-10" aria-label="Loading" />}
      {error && <Error />}
      {!loading && tv && (
        <div>
          <Header imgUrl={tv.poster_path} name={tv.name} link={`/tv/${id}`} />
          <Cast cast={cast?.cast} crew={cast?.crew} />
        </div>
      )}
    </>
  );
};

export default TVCast;
