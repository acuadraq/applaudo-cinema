import React from 'react';
import { Link } from 'react-router-dom';
import routesNames from '../../routes/customRoutes';
import notFoundSVG from '../../static/img/notFound.svg';

const NotFound = () => {
  return (
    <div className="container mx-auto flex px-5 py-24 items-center justify-center flex-col">
      <img
        src={notFoundSVG}
        alt="Not Found"
        className="lg:w-2/6 md:w-3/6 w-5/6 mb-10 object-cover object-center rounded"
      />

      <div className="text-center lg:w-2/3 w-full">
        <h1 className="text-2xl mb-4 font-medium text-palidcolor">
          Looks like you have found the doorway to the great nothing
        </h1>
        <p className="mb-8 leading-relaxed text-palidcolor">
          Sorry about that! Please visit the homepage to get where you need to go
        </p>
        <div className="flex justify-center">
          <Link
            to={routesNames.home}
            className=" text-palidcolor bg-cardscolor uppercase font-bold border-0 py-2 px-6 focus:outline-none hover:bg-cardshover rounded text-lg"
          >
            Go home
          </Link>
        </div>
      </div>
    </div>
  );
};

export default NotFound;
