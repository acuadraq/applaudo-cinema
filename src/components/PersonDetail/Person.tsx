import React, { useState, useEffect } from 'react';
import { LazyLoadImage } from 'react-lazy-load-image-component';
import 'react-lazy-load-image-component/src/effects/blur.css';
import { Link, useParams } from 'react-router-dom';
import { getPersonCredits, getPersonInfo } from '../../adapters/Fetch';
import { getPersonProfile } from '../../adapters/Image';
import { ICreditsPerson, IPerson } from '../../interfaces/person';
import Error from '../Error/Error';

const Person = () => {
  const [personInfo, setPersonInfo] = useState<IPerson>();
  const [credits, setCredits] = useState<ICreditsPerson>();
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<boolean>(false);
  const { id } = useParams();

  const getCredits = async () => {
    if (id) {
      try {
        const info: ICreditsPerson = await getPersonCredits(parseInt(id, 10));
        setCredits(info);
        setLoading(false);
      } catch (err) {
        setError(true);
        setLoading(false);
      }
    }
  };

  const getInfo = async () => {
    if (id) {
      try {
        const info: IPerson = await getPersonInfo(parseInt(id, 10));
        setPersonInfo(info);
        getCredits();
      } catch (err) {
        setError(true);
        setLoading(false);
      }
    }
  };

  useEffect(() => {
    getInfo();
  }, []);
  return (
    <>
      {loading && <div className="animation__loading mt-10" aria-label="Loading" />}
      {error && <Error />}
      {personInfo && !loading && (
        <div className="big__container">
          <div className="grid md:grid-cols-4 gap-10 text-palidcolor">
            <div className="text-center md:text-left">
              <LazyLoadImage
                src={
                  personInfo.profile_path
                    ? getPersonProfile(personInfo.profile_path)
                    : 'https://i.imgur.com/jg9UKHI.png'
                }
                alt={personInfo.name}
                className="rounded-md mx-auto"
                effect="blur"
                width="300"
                height="450"
              />
              <h2 className="mt-5 font-bold text-xl">Personal Info</h2>
              <h3 className="mt-3 font-semibold">Known For</h3>
              <p className="mt-1">{personInfo.known_for_department}</p>
              {personInfo.birthday && (
                <>
                  <h3 className="mt-3 font-semibold">Birthday</h3>
                  <p className="mt-1">{personInfo.birthday}</p>
                </>
              )}
              {personInfo.gender && (
                <>
                  <h3 className="mt-3 font-semibold">Gender</h3>
                  <p className="mt-1">
                    {personInfo.gender === 2 ? 'Male' : personInfo.gender === 1 ? 'Female' : '----'}
                  </p>
                </>
              )}
              {personInfo.place_of_birth && (
                <>
                  <h3 className="mt-3 font-semibold">Place Of Birth</h3>
                  <p className="mt-1">{personInfo.place_of_birth}</p>
                </>
              )}
              {personInfo.deathday && (
                <>
                  <h3 className="mt-3 font-semibold">Death Day</h3>
                  <p className="mt-1">{personInfo.deathday}</p>
                </>
              )}
            </div>
            <div className="md:col-span-3">
              <h1 className="text-2xl font-bold mt-5">{personInfo.name}</h1>
              <h2 className="mt-5 text-lg font-semibold">Biography</h2>
              <p className="mt-5 max-h-96 overflow-auto scroll__container pr-3">
                {personInfo.biography
                  ? personInfo.biography
                  : `We don't have a biography for ${personInfo.name}`}
              </p>
              {credits && (
                <>
                  {credits.cast.length === 0 && credits.crew.length === 0 && (
                    <p>This person does not have their career added</p>
                  )}
                  <h2 className="mt-5 text-lg font-semibold">A little bit about the career Life</h2>
                  <div className="shadow-2xl rounded-lg mt-3">
                    {credits.cast.length > 0 && (
                      <>
                        <h3 className="mt-3">Acting</h3>
                        {credits.cast.slice(0, 10).map(credit => (
                          <div key={credit.id}>
                            <div className="flex flex-wrap p-5">
                              <span>
                                {credit.release_date ? credit.release_date.slice(0, 4) : '----'}
                              </span>
                              <span className="mx-3 md:mx-10">•</span>
                              <div>
                                <Link
                                  to={`/${credit.media_type}/${credit.id}`}
                                  className="font-bold hover:text-hovertext"
                                >
                                  {credit.name ? credit.name : credit.title}
                                </Link>
                                {credit.character && (
                                  <span className="ml-1 text-hovertext">as {credit.character}</span>
                                )}
                              </div>
                            </div>
                            <hr />
                          </div>
                        ))}
                      </>
                    )}
                  </div>
                  {credits.crew.length > 0 && (
                    <>
                      <h3 className="mt-3">Crew</h3>
                      <div className="shadow-2xl rounded-lg mt-3">
                        {credits?.crew.slice(0, 10).map(credit => (
                          <div key={credit.id}>
                            <div className="flex flex-wrap p-5">
                              <span>
                                {credit.release_date ? credit.release_date.slice(0, 4) : '----'}
                              </span>
                              <span className="mx-3 md:mx-10">•</span>
                              <div>
                                <Link
                                  to={`/${credit.media_type}/${credit.id}`}
                                  className="font-bold hover:text-hovertext"
                                >
                                  {credit.name ? credit.name : credit.title}
                                </Link>
                                {credit.job && (
                                  <span className="ml-1 text-hovertext">as {credit.job}</span>
                                )}
                              </div>
                            </div>
                            <hr />
                          </div>
                        ))}
                      </div>
                    </>
                  )}
                </>
              )}
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default Person;
