import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { getCast, getMovieDetail } from '../../adapters/MoviesFetch';
import { ICast } from '../../interfaces/credits';
import IMovie from '../../interfaces/movie';
import Cast from '../Detail/Cast';
import Header from '../Detail/Header';
import Error from '../Error/Error';

const MovieCast = () => {
  const [cast, setCast] = useState<ICast>();
  const [movie, setMovie] = useState<IMovie>();
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<boolean>(false);
  const { id } = useParams<string>();

  const getMovieCast = async () => {
    if (id) {
      try {
        const list: ICast = await getCast(parseInt(id, 10));
        setCast(list);
        setLoading(false);
      } catch (err) {
        setError(true);
        setLoading(false);
      }
    }
  };

  const getDetails = async () => {
    if (id) {
      try {
        const info: IMovie = await getMovieDetail(parseInt(id, 10));
        setMovie(info);
        getMovieCast();
      } catch (err) {
        setError(true);
        setLoading(false);
      }
    }
  };

  useEffect(() => {
    getDetails();
  }, []);

  return (
    <>
      {loading && <div className="animation__loading mt-10" aria-label="Loading" />}
      {error && <Error />}
      {!loading && movie && (
        <div>
          <Header imgUrl={movie.poster_path} name={movie.title} link={`/movie/${id}`} />
          <Cast cast={cast?.cast} crew={cast?.crew} />
        </div>
      )}
    </>
  );
};

export default MovieCast;
