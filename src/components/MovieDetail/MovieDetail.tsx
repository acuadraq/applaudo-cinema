import React, { useState, useEffect, useContext } from 'react';
import { useParams } from 'react-router-dom';
import {
  checkIsInFavorites,
  getCast,
  getImagesMovies,
  getMovieDetail,
  getReviews,
  getSimilar,
} from '../../adapters/MoviesFetch';
import { MyContext } from '../../context/context';
import { ICast } from '../../interfaces/credits';
import { IImages } from '../../interfaces/images';
import { IIsInFavorites, IStoredAccount } from '../../interfaces/login';
import IMovie from '../../interfaces/movie';
import { IMovies } from '../../interfaces/movies';
import { IReview } from '../../interfaces/reviews';
import DetailHeader from '../Detail/DetailHeader';
import DetailInfo from '../Detail/DetailInfo';
import Error from '../Error/Error';

const MovieDetail = () => {
  const [movie, setMovie] = useState<IMovie>();
  const [similar, setSimilar] = useState<IMovies>();
  const [reviews, setReviews] = useState<IReview>();
  const [images, setImages] = useState<IImages>();
  const [isInFavorites, setIsInFavorites] = useState<boolean | string>('NOT');
  const [cast, setCast] = useState<ICast>();
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<boolean>(false);
  const { id } = useParams<string>();
  const isLogged = useContext(MyContext);

  const getFavorites = async () => {
    const { sessionId } = isLogged as IStoredAccount;
    if (id) {
      try {
        const checking: IIsInFavorites = await checkIsInFavorites(parseInt(id, 10), sessionId);
        if (checking.favorite) {
          setIsInFavorites(true);
          setLoading(false);
        } else {
          setIsInFavorites(false);
          setLoading(false);
        }
      } catch (err) {
        setError(true);
        setLoading(false);
      }
    }
  };

  const getMovieReview = async () => {
    if (id) {
      try {
        const info: IReview = await getReviews(parseInt(id, 10));
        setReviews(info);
        if (isLogged) {
          getFavorites();
        } else {
          setLoading(false);
        }
      } catch (err) {
        setLoading(false);
      }
    }
  };

  const getMoviesSimilar = async () => {
    if (id) {
      try {
        const info: IMovies = await getSimilar(parseInt(id, 10));
        setSimilar(info);
        getMovieReview();
      } catch (err) {
        getMovieReview();
      }
    }
  };

  const getImages = async () => {
    if (id) {
      try {
        const info: IImages = await getImagesMovies(parseInt(id, 10));
        setImages(info);
        getMoviesSimilar();
      } catch (err) {
        getMoviesSimilar();
      }
    }
  };

  const getMovieCast = async () => {
    if (id) {
      try {
        const info: ICast = await getCast(parseInt(id, 10));
        setCast(info);
        getImages();
      } catch (err) {
        setError(true);
        setLoading(false);
      }
    }
  };

  const getDetails = async () => {
    if (id) {
      setLoading(true);
      try {
        const info: IMovie = await getMovieDetail(parseInt(id, 10));
        setMovie(info);
        getMovieCast();
      } catch (err) {
        setError(true);
        setLoading(false);
      }
    }
  };

  useEffect(() => {
    getDetails();
  }, [id]);

  return (
    <>
      {loading && <div className="animation__loading mt-10" aria-label="Loading" />}
      {error && <Error />}
      {movie && !loading && !error && (
        <>
          <DetailHeader
            movie={movie}
            cast={cast}
            favorite={isInFavorites}
            type="movie"
            getFavorites={getFavorites}
          />
          <div className="big__container">
            <DetailInfo
              crew={cast}
              images={images}
              similarMovies={similar}
              reviews={reviews}
              type="movie"
            />
          </div>
        </>
      )}
    </>
  );
};

export default MovieDetail;
