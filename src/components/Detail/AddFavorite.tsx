import React, { useContext, useState } from 'react';
import { MyContext } from '../../context/context';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.min.css';
import { IPostFavorite, IStoredAccount } from '../../interfaces/login';
import addOrRemove from '../../helpers/favoritesHandlers';
import Modal from '../Modal/Modal';

type favoriteProps = {
  favorite: string | boolean;
  type: string;
  id: number | undefined;
  getFavorites: () => void;
};

const AddFavorite = ({ favorite, type, id, getFavorites }: favoriteProps) => {
  const isLogged = useContext(MyContext);
  const { id: accountId, sessionId } = isLogged as IStoredAccount;
  const [open, setOpen] = useState<boolean>(false);

  const remove = async () => {
    setOpen(!open);
    const submitObject: IPostFavorite = {
      'media_type': type,
      'media_id': id,
      'favorite': false,
    };
    const result = await addOrRemove(submitObject, accountId, sessionId, 'remove');
    if (result?.success) getFavorites();
  };

  const add = async () => {
    const submitObject: IPostFavorite = {
      'media_type': type,
      'media_id': id,
      'favorite': true,
    };
    const result = await addOrRemove(submitObject, accountId, sessionId, 'add');
    if (result?.success) getFavorites();
  };

  const openCloseModal = () => setOpen(!open);

  return (
    <>
      <Modal openModal={open} openCloseModal={openCloseModal} remove={remove} />
      {favorite === false && (
        <>
          <button onClick={add}>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className={`h-6 w-6 group-hover:text-buttonscolor transform ease-in duration-150 hover:scale-110`}
              viewBox="0 0 20 20"
              fill="currentColor"
            >
              <path
                fillRule="evenodd"
                d="M3.172 5.172a4 4 0 015.656 0L10 6.343l1.172-1.171a4 4 0 115.656 5.656L10 17.657l-6.828-6.829a4 4 0 010-5.656z"
                clipRule="evenodd"
              />
            </svg>
          </button>
          <button className="ml-2 cursor-pointer" onClick={add}>
            Add To Favorites
          </button>
        </>
      )}
      {favorite === true && (
        <>
          <button onClick={openCloseModal}>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className={`h-6 w-6 text-buttonscolor transform ease-in duration-150 hover:scale-110`}
              viewBox="0 0 20 20"
              fill="currentColor"
            >
              <path
                fillRule="evenodd"
                d="M3.172 5.172a4 4 0 015.656 0L10 6.343l1.172-1.171a4 4 0 115.656 5.656L10 17.657l-6.828-6.829a4 4 0 010-5.656z"
                clipRule="evenodd"
              />
            </svg>
          </button>
          <button onClick={openCloseModal} className="ml-2 cursor-pointer">
            Remove from Favorites
          </button>
        </>
      )}
      {favorite === 'NOT' && (
        <>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className={`h-6 w-6 text-hovertext`}
            viewBox="0 0 20 20"
            fill="currentColor"
          >
            <path
              fillRule="evenodd"
              d="M3.172 5.172a4 4 0 015.656 0L10 6.343l1.172-1.171a4 4 0 115.656 5.656L10 17.657l-6.828-6.829a4 4 0 010-5.656z"
              clipRule="evenodd"
            />
          </svg>
          <span className="ml-2 text-hovertext">Log In to add to favorites</span>
        </>
      )}
      <ToastContainer
        position="top-center"
        autoClose={3000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
    </>
  );
};

export default AddFavorite;
