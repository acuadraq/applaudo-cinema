import React from 'react';
import { LazyLoadImage } from 'react-lazy-load-image-component';
import 'react-lazy-load-image-component/src/effects/blur.css';
import { getFullImage, getPosterImage } from '../../adapters/Image';
import { ICast } from '../../interfaces/credits';
import IMovie from '../../interfaces/movie';
import { IShow } from '../../interfaces/tv';
import AddFavorite from './AddFavorite';

type headerProps = {
  movie?: IMovie;
  tv?: IShow;
  cast?: ICast;
  favorite: string | boolean;
  type: string;
  getFavorites: () => void;
};

const DetailHeader = ({ movie, tv, cast, favorite, type, getFavorites }: headerProps) => {
  return (
    <div
      style={{
        background: `linear-gradient(rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.7)), url(${getFullImage(
          movie ? movie.backdrop_path : tv?.backdrop_path,
        )})`,
        backgroundSize: `cover`,
        backgroundRepeat: `no-repeat`,
        backgroundPosition: `center top`,
      }}
      className="h-auto w-full"
    >
      <div className="big__container">
        <div className="flex flex-wrap justify-around md:grid md:grid-cols-3 lg:grid-cols-4 gap-10 2xl:gap-0">
          <div className="image_container">
            <LazyLoadImage
              effect="blur"
              src={
                movie?.poster_path
                  ? getPosterImage(movie.poster_path)
                  : tv?.poster_path
                  ? getPosterImage(tv.poster_path)
                  : 'https://i.imgur.com/XVGMFoR.png'
              }
              alt={movie ? movie.title : tv?.name}
              className="rounded-lg"
              width="300"
              height="400"
            />
          </div>
          <div className="md:col-span-2 lg:col-span-3 text-palidcolor mt-3">
            <h1 className="font-bold text-2xl lg:text-4xl">{movie ? movie.title : tv?.name}</h1>
            <div className="mt-2">
              {movie && <span>{movie?.release_date.replaceAll('-', '/')}</span>}
              {tv && <span>{tv.number_of_episodes} EP</span>}
              <span className="mx-2">•</span>
              <span>
                {movie
                  ? movie?.genres.map(genre => genre.name).join(' | ')
                  : tv?.genres.map(genre => genre.name).join(' | ')}
              </span>
              <span className="mx-2">•</span>
              <span>{movie ? movie.runtime : tv?.episode_run_time[0]} min</span>
            </div>
            <div className="mt-3 flex items-center">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="h-7 w-7"
                viewBox="0 0 20 20"
                fill="currentColor"
              >
                <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z" />
              </svg>
              <span className="ml-1">{movie ? movie.vote_average : tv?.vote_average}</span>
              <div className="flex items-center ml-5 group">
                <AddFavorite
                  favorite={favorite}
                  type={type}
                  id={movie ? movie.id : tv?.id}
                  getFavorites={getFavorites}
                />
              </div>
            </div>
            <h2 className="text-lg font-bold mt-5">Overview</h2>
            <p className="mt-2">{movie ? movie.overview : tv?.overview}</p>
            <div className="mt-5 flex justify-start flex-wrap">
              {cast?.crew
                .filter(person => person.department === 'Writing')
                .slice(0, 8)
                .map((person, index) => (
                  <div key={index} className="mr-2 my-2 md:mr-8 md:my-5">
                    <h5 className="font-bold">{person.name}</h5>
                    <span>{person.job}</span>
                  </div>
                ))}
              {tv?.created_by.map(creator => (
                <div key={creator.id} className="mr-2 my-2 md:mr-8 md:my-5">
                  <h5 className="font-bold">{creator.name}</h5>
                  <span>Creator</span>
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default DetailHeader;
