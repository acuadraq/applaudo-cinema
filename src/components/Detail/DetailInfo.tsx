import React from 'react';
import { LazyLoadImage } from 'react-lazy-load-image-component';
import 'react-lazy-load-image-component/src/effects/blur.css';
import { Link } from 'react-router-dom';
import { getPosterImage, getSeasonURL, getW500 } from '../../adapters/Image';
import { ICast } from '../../interfaces/credits';
import { IImages } from '../../interfaces/images';
import { IMovies } from '../../interfaces/movies';
import { IReview } from '../../interfaces/reviews';
import { IShow } from '../../interfaces/tv';
import { ITvShows } from '../../interfaces/tvshows';
import avatar from '../../static/img/avatar.png';
import Card from '../Card/Card';

type detailProps = {
  crew: ICast | undefined;
  images: IImages | undefined;
  similarMovies?: IMovies | undefined;
  similarTvs?: ITvShows | undefined;
  tvinfo?: IShow | undefined;
  reviews: IReview | undefined;
  type: string;
};

const DetailInfo = ({
  crew,
  images,
  similarMovies,
  reviews,
  type,
  similarTvs,
  tvinfo,
}: detailProps) => {
  const getSeasonNumber = () => {
    if (tvinfo) {
      const season = tvinfo.last_episode_to_air.season_number;
      return tvinfo?.seasons.map(season => season.season_number).indexOf(season);
    }
    return 0;
  };
  return (
    <div>
      <section>
        <div className="flex justify-between">
          <h2 className="section__title">Movie Cast</h2>
          <Link to="cast" className="text-palidcolor hover:text-hovertext">
            View All
          </Link>
        </div>
        <div className="scroll__div">
          {crew && (
            <>
              {crew.cast.slice(0, 6).map(cast => (
                <div key={cast.id} className="scroll-div__child">
                  <Card
                    id={cast.id}
                    title={cast.name}
                    imgUrl={cast.profile_path ? getPosterImage(cast.profile_path) : null}
                    type="/person"
                    searchType={cast.character}
                  />
                </div>
              ))}
              {crew.cast.length === 0 && (
                <p className="mt-5 text-palidcolor text-lg">We do not have any cast added</p>
              )}
            </>
          )}
        </div>
      </section>
      {type === 'tv show' && tvinfo?.last_episode_to_air && (
        <section className="mt-10">
          <h2 className="section__title" aria-label="Last Season">
            Last Season
          </h2>
          <div className="mt-5">
            <div className="flex flex-wrap rounded-md shadow-lg overflow-hidden bg-cardscolor w-full md:w-1/2 mb-5">
              <div className="flex flex-wrap box-border">
                <div className="w-seasonwidth h-seasonheight min-w-seasonwidth box-border mx-auto">
                  <Link to={`seasons/${tvinfo?.seasons[getSeasonNumber()].season_number}`}>
                    <LazyLoadImage
                      src={
                        tvinfo?.seasons[getSeasonNumber()].poster_path
                          ? getSeasonURL(tvinfo?.seasons[getSeasonNumber()].poster_path)
                          : 'https://i.imgur.com/Nd0rpzx.png'
                      }
                    />
                  </Link>
                </div>
                <div className="text-palidcolor py-7 px-5">
                  <Link
                    to={`seasons/${tvinfo?.seasons[getSeasonNumber()].season_number}`}
                    className="text-xl uppercase hover:text-hovertext"
                  >
                    {tvinfo?.seasons[getSeasonNumber()].name}
                  </Link>
                  <h4 className="mt-1">
                    <span>{tvinfo?.seasons[getSeasonNumber()].air_date.slice(0, 4)}</span> |{' '}
                    {tvinfo?.seasons[getSeasonNumber()].episode_count} EP
                  </h4>
                  <p className="mt-5">
                    {`Season ${tvinfo?.seasons[getSeasonNumber()].season_number} of ${
                      tvinfo?.name
                    }`}
                  </p>
                </div>
              </div>
            </div>
            <Link to={`/tv/${tvinfo?.id}/seasons`} className="text-palidcolor hover:text-hovertext">
              View All Seasons
            </Link>
          </div>
        </section>
      )}
      {images && images.backdrops.length > 0 && (
        <section className="mt-10">
          <h2 className="section__title">{type} Images</h2>
          <div className="grid sm:grid-cols-2 lg:grid-cols-3 gap-10 mt-5">
            {images?.backdrops.slice(0, 6).map((image, index) => (
              <LazyLoadImage
                aria-label={`img-${index}`}
                key={index}
                src={getW500(image.file_path)}
                alt="Movie Image"
                effect="blur"
                className="object-cover w-full h-full bg-center"
              />
            ))}
          </div>
        </section>
      )}
      {similarMovies && similarMovies.results.length > 0 && (
        <section className="mt-10">
          <h2 className="section__title">Similar Movies</h2>
          <div className="scroll__div">
            {similarMovies.results.slice(0, 7).map(movie => (
              <div className="scroll-div__child" key={movie.id}>
                <Card
                  id={movie.id}
                  title={movie.title}
                  imgUrl={movie.poster_path ? getPosterImage(movie.poster_path) : null}
                  type="/movie"
                />
              </div>
            ))}
          </div>
        </section>
      )}
      {similarTvs && similarTvs.results.length > 0 && (
        <section className="mt-10">
          <h2 className="section__title">Similar TV Shows</h2>
          <div className="scroll__div">
            {similarTvs.results.slice(0, 7).map(tv => (
              <div className="scroll-div__child" key={tv.id}>
                <Card
                  id={tv.id}
                  title={tv.name}
                  imgUrl={tv.poster_path ? getPosterImage(tv.poster_path) : null}
                  type="/tv"
                />
              </div>
            ))}
          </div>
        </section>
      )}
      {reviews && (
        <section className="mt-10">
          <h2 className="section__title mb-5">Reviews</h2>
          {reviews?.results.length === 0 && (
            <p className="mt-3 text-lg text-palidcolor">This {type} does not have reviews yet</p>
          )}
          <div className="grid grid-cols-1 md:grid-cols-2 text-palidcolor gap-10">
            {reviews?.results.slice(0, 6).map(review => (
              <div key={review.id} className="bg-cardscolor p-5 rounded-lg shadow-md">
                <div className="flex justify-between">
                  <div className="flex items-center mb-1">
                    <img src={avatar} alt="Avatar" className="w-8 h-8 rounded-full mr-2" />
                    <span>{review.author}</span>
                  </div>
                  <div className="flex items-center">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      className="h-5 w-5 mr-1"
                      viewBox="0 0 20 20"
                      fill="currentColor"
                    >
                      <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z" />
                    </svg>
                    <span>{review.author_details.rating ? review.author_details.rating : 0}</span>
                  </div>
                </div>
                <hr />
                <div className="mt-1">
                  <p>{review.content.slice(0, 200)}</p>
                </div>
              </div>
            ))}
          </div>
        </section>
      )}
    </div>
  );
};

export default DetailInfo;
