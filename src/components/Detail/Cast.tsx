import React, { useState } from 'react';
import { ICasts, ICrew } from '../../interfaces/credits';
import { getProfilePic } from '../../adapters/Image';
import ReactPaginate from 'react-paginate';
import { LazyLoadImage } from 'react-lazy-load-image-component';
import 'react-lazy-load-image-component/src/effects/blur.css';
import { Link } from 'react-router-dom';

type castProps = {
  cast: ICasts[] | undefined;
  crew: ICrew[] | undefined;
};

const Cast = ({ cast, crew }: castProps) => {
  const [pageNumber, setPageNumber] = useState<number>(0);
  const peoplePerPage = 12;
  const pagesVisited = pageNumber * peoplePerPage;
  let pageCount: number = 0;

  const currentCast = cast?.slice(pagesVisited, pagesVisited + peoplePerPage);

  if (cast) pageCount = Math.ceil(cast.length / peoplePerPage);

  const changePage = (selected: number) => setPageNumber(selected);

  const getPersonInfo = (
    id: number,
    name: string,
    job: string,
    img?: string | undefined | null,
  ) => {
    return (
      <>
        <Link to={`/person/${id}`}>
          <LazyLoadImage
            src={img ? getProfilePic(img) : 'https://i.imgur.com/4wYDLvH.png'}
            alt={name}
            className="rounded-lg w-16 h-16"
            width="66"
            effect="blur"
            height="66"
          />
        </Link>

        <div className="col-span-3 md:col-span-4 xl:col-span-7 flex break-words flex-wrap content-center items-center ml-5">
          <Link to={`/person/${id}`}>
            <h4 className="font-bold hover:text-hovertext">{name}</h4>
          </Link>
          <h5 className="text-sm text-hovertext w-full">{job}</h5>
        </div>
      </>
    );
  };

  return (
    <section className="big__container text-palidcolor grid md:grid-cols-2">
      <div>
        <h2 className="section__title">Cast {cast?.length}</h2>
        <div className="mt-5">
          {currentCast && (
            <>
              {currentCast.length > 0 && (
                <>
                  {currentCast.map(person => (
                    <div
                      key={person.id}
                      className="mb-3 grid grid-cols-4 md:grid-cols-5 xl:grid-cols-8 text-left w-full"
                    >
                      {getPersonInfo(person.id, person.name, person.character, person.profile_path)}
                    </div>
                  ))}
                  <ReactPaginate
                    previousLabel={'<'}
                    nextLabel={'>'}
                    pageCount={pageCount}
                    pageRangeDisplayed={2}
                    marginPagesDisplayed={1}
                    onPageChange={e => changePage(e.selected)}
                    containerClassName={'container__pagination'}
                    pageClassName={'md:mx-5'}
                    pageLinkClassName={'page__link'}
                    activeLinkClassName={'bg-cardscolor font-bold'}
                    previousLinkClassName={'previous__after'}
                    nextLinkClassName={'previous__after'}
                    disabledClassName={'prev__after--disabled'}
                  />
                </>
              )}
              {currentCast.length === 0 && <p>There are no cast added</p>}
            </>
          )}
        </div>
      </div>
      <div>
        <h2 className="section__title">Crew {crew?.length}</h2>
        <div className="mt-5 overflow-auto h-sliderHeight scroll__container">
          {crew && (
            <>
              {crew.length === 0 && <p>There are no crew added</p>}
              {crew.filter(person => person.department === 'Art').length > 0 && (
                <>
                  <h3 className="text-lg mb-2">Art</h3>
                  {crew
                    ?.filter(person => person.department === 'Art')
                    .map(people => (
                      <div
                        key={people.id}
                        className="mb-3 grid grid-cols-4 md:grid-cols-5 xl:grid-cols-8 text-left w-full"
                      >
                        {getPersonInfo(people.id, people.name, people.job, people.profile_path)}
                      </div>
                    ))}
                </>
              )}
              {crew?.filter(person => person.department === 'Camera').length > 0 && (
                <>
                  <h3 className="text-lg mb-3">Camera</h3>
                  {crew
                    ?.filter(person => person.department === 'Camera')
                    .map(people => (
                      <div
                        key={people.id}
                        className="mb-3 grid grid-cols-4 md:grid-cols-5 xl:grid-cols-8 text-left w-full"
                      >
                        {getPersonInfo(people.id, people.name, people.job, people.profile_path)}
                      </div>
                    ))}
                </>
              )}
              {crew?.filter(person => person.department === 'Costume & Make-Up').length > 0 && (
                <>
                  <h3 className="text-lg mb-3">Costume & Make-Up</h3>
                  {crew
                    ?.filter(person => person.department === 'Costume & Make-Up')
                    .slice(0, 10)
                    .map(people => (
                      <div
                        key={people.id}
                        className="mb-3 grid grid-cols-4 md:grid-cols-5 xl:grid-cols-8 text-left w-full"
                      >
                        {getPersonInfo(people.id, people.name, people.job, people.profile_path)}
                      </div>
                    ))}
                </>
              )}
              {crew?.filter(person => person.department === 'Crew').length > 0 && (
                <>
                  <h3 className="text-lg mb-3">Crew</h3>
                  {crew
                    ?.filter(person => person.department === 'Crew')
                    .map(people => (
                      <div
                        key={people.id}
                        className="mb-3 grid grid-cols-4 md:grid-cols-5 xl:grid-cols-8 text-left w-full"
                      >
                        {getPersonInfo(people.id, people.name, people.job, people.profile_path)}
                      </div>
                    ))}
                </>
              )}
              {crew?.filter(person => person.department === 'Directing').length > 0 && (
                <>
                  <h3 className="text-lg mb-3">Directing</h3>
                  {crew
                    ?.filter(person => person.department === 'Directing')
                    .map(people => (
                      <div
                        key={people.id}
                        className="mb-3 grid grid-cols-4 md:grid-cols-5 xl:grid-cols-8 text-left w-full"
                      >
                        {getPersonInfo(people.id, people.name, people.job, people.profile_path)}
                      </div>
                    ))}
                </>
              )}
              {crew?.filter(person => person.department === 'Editing').length > 0 && (
                <>
                  <h3 className="text-lg mb-3">Editing</h3>
                  {crew
                    ?.filter(person => person.department === 'Editing')
                    .map(people => (
                      <div
                        key={people.id}
                        className="mb-3 grid grid-cols-4 md:grid-cols-5 xl:grid-cols-8 text-left w-full"
                      >
                        {getPersonInfo(people.id, people.name, people.job, people.profile_path)}
                      </div>
                    ))}
                </>
              )}
              {crew?.filter(person => person.department === 'Production').length > 0 && (
                <>
                  <h3 className="text-lg mb-3">Production</h3>
                  {crew
                    ?.filter(person => person.department === 'Production')
                    .map(people => (
                      <div
                        key={people.id}
                        className="mb-3 grid grid-cols-4 md:grid-cols-5 xl:grid-cols-8 text-left w-full"
                      >
                        {getPersonInfo(people.id, people.name, people.job, people.profile_path)}
                      </div>
                    ))}
                </>
              )}
            </>
          )}
        </div>
      </div>
    </section>
  );
};

export default Cast;
