import React from 'react';
import { LazyLoadImage } from 'react-lazy-load-image-component';
import 'react-lazy-load-image-component/src/effects/blur.css';
import { Link } from 'react-router-dom';
import { getSmallerPoster } from '../../adapters/Image';

type headerProps = {
  imgUrl: string | null;
  name: string;
  link: string;
};

const Header = ({ imgUrl, name, link }: headerProps) => {
  return (
    <section className="bg-bgcolor w-full">
      <div className="xl:container px-4 sm:px-8 lg:px-16 xl:px-24 mx-auto py-5">
        <div className="flex items-center">
          <LazyLoadImage
            src={imgUrl ? getSmallerPoster(imgUrl) : 'https://i.imgur.com/Nd0rpzx.png'}
            alt={name}
            effect="blur"
            width="58"
            height="87"
          />

          <div className="ml-5">
            <h1 className="text-2xl text-palidcolor font-bold mb-1">{name}</h1>
            <Link to={link} className="text-hovertext hover:text-palidcolor flex items-center">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="h-5 w-5 mr-1"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="M7 16l-4-4m0 0l4-4m-4 4h18"
                />
              </svg>
              Go Back
            </Link>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Header;
