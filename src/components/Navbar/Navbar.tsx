import React, { useContext, useState, useEffect } from 'react';
import routesNames from '../../routes/customRoutes';
import { useNavigate } from 'react-router-dom';
import { Link } from 'react-router-dom';
import { debounce } from 'lodash';
import { ToastContainer, toast } from 'react-toastify';
import { MyContext } from '../../context/context';
import { IStoredAccount } from '../../interfaces/login';
import { deleteSession } from '../../adapters/Login';
import useLocalStorage from '../../customhooks/useStorage';

const Navbar = () => {
  const navigate = useNavigate();
  const isLogged = useContext(MyContext);
  const [isLog, setIsLog] = useLocalStorage('sessionId', false);
  const { name, sessionId } = isLogged as IStoredAccount;
  const [openDropdown, setOpenDropdown] = useState<boolean>(false);

  const handleSearch = (searchValue: string) => {
    if (searchValue.trim() === '') return;
    navigate(`/search?query=${searchValue}`);
  };

  const logOut = async () => {
    try {
      const submitLogout = await deleteSession({ session_id: sessionId });
      if (submitLogout.success) {
        setIsLog(false);
        window.location.reload();
      } else {
        toast.error('There was an error during log out');
      }
    } catch (err) {
      toast.error('There was an error during log out');
    }
  };

  useEffect(() => {
    setOpenDropdown(false);
  }, [window.location.pathname]);

  const searchFunction = debounce(handleSearch, 900);

  return (
    <>
      <header className="text-palidcolor bg-bgcolor sticky top-0 z-10">
        <div className="container mx-auto flex flex-wrap p-5 flex-col md:flex-row items-center">
          <Link to={routesNames.home} className="flex mb-4 md:mb-0">
            <span className="ml-3 text-xl font-bold">Applaudo Cinema</span>
          </Link>

          <div className="mb-4 md:mb-0 md:mr-auto md:ml-auto md:w-1/4">
            <input
              type="text"
              placeholder="Movie, TV Show, people"
              className="bg-bgcolor border-b outline-none mr-3 w-full"
              onChange={e => searchFunction(e.target.value)}
            />
          </div>
          <nav className="flex flex-wrap">
            <Link to={routesNames.movies} className="mr-5 hover:text-hovertext">
              Movies
            </Link>
            <Link to={routesNames.shows} className="mr-5 hover:text-hovertext">
              TV Shows
            </Link>
            {isLogged && (
              <div className="relative inline-block">
                <button
                  onClick={() => setOpenDropdown(!openDropdown)}
                  className="hover:text-hovertext flex justify-center items-center"
                >
                  {name}
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="h-4 w-4 ml-1"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth="2"
                      d="M19 9l-7 7-7-7"
                    />
                  </svg>
                </button>
                <div
                  className={`${
                    openDropdown ? 'block' : 'hidden'
                  } absolute bg-bgsecondcolor w-28 overflow-auto z-2`}
                >
                  <Link
                    to={routesNames.favorites}
                    className="w-full py-3 px-4 block hover:bg-gray-800"
                  >
                    Favorites
                  </Link>
                  <hr />
                  <a
                    onClick={logOut}
                    className="w-full cursor-pointer py-3 px-4 block hover:bg-gray-800"
                  >
                    Log Out
                  </a>
                </div>
              </div>
            )}
            {!isLogged && (
              <Link to={routesNames.login} className="hover:text-hovertext">
                Log in
              </Link>
            )}
          </nav>
        </div>
      </header>
      <ToastContainer
        position="top-center"
        autoClose={3000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
    </>
  );
};

export default Navbar;
