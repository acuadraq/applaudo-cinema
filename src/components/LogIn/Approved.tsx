import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { getAccountInfo, postSession } from '../../adapters/Login';
import useStorage from '../../customhooks/useStorage';
import { IAccount, ISessionPost, IStoredAccount } from '../../interfaces/login';
import Error from '../Error/Error';

const Approved = () => {
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<boolean>(false);
  const [isLogged, setLoggin] = useStorage('sessionId', false);
  const navigate = useNavigate();
  const urlSearch = new URL(window.location.href);

  const saveToLocal = async (sessionId: string) => {
    try {
      const accountInformation: IAccount = await getAccountInfo(sessionId);
      const account: IStoredAccount = {
        id: accountInformation.id,
        name: accountInformation.name,
        username: accountInformation.username,
        avatar: accountInformation.avatar.tmdb.avatar_path,
        sessionId: sessionId,
      };
      setLoggin(account);
      navigate('/favorites');
    } catch (err) {
      setError(true);
      setLoading(false);
    }
  };

  const getSession = async () => {
    try {
      const info: ISessionPost = {
        'request_token': urlSearch.searchParams.get('request_token'),
      };
      const infoSent = await postSession(info);
      if (infoSent.success) {
        saveToLocal(infoSent.session_id);
      } else {
        setError(true);
        setLoading(false);
      }
    } catch (err) {
      setError(true);
      setLoading(false);
    }
  };

  useEffect(() => {
    getSession();
  }, []);

  return (
    <>
      {loading && <div className="animation__loading mt-5" aria-label="Loading" />}
      {error && <Error />}
    </>
  );
};

export default Approved;
