import React, { useState } from 'react';
import logInSvg from '../../static/img/LogIn.svg';
import { generateToken } from '../../adapters/Login';
import { INewToken } from '../../interfaces/login';
import Error from '../Error/Error';

const LogIn = () => {
  const [error, setError] = useState<boolean>(false);
  const { REACT_APP_WEB_URL } = process.env;

  const handleLogin = async () => {
    try {
      const token: INewToken = await generateToken();
      window.location.replace(
        `https://www.themoviedb.org/authenticate/${token.request_token}?redirect_to=${REACT_APP_WEB_URL}`,
      );
    } catch (err) {
      setError(true);
    }
  };

  return (
    <>
      {error && <Error />}
      {!error && (
        <div className="container mx-auto flex px-5 py-24 items-center justify-center flex-col">
          <img
            src={logInSvg}
            alt="Not Found"
            className="lg:w-2/6 md:w-3/6 w-5/6 mb-10 object-cover object-center rounded"
          />

          <div className="text-center lg:w-2/3 w-full">
            <h1 className="text-2xl mb-4 text-palidcolor font-bold">Do you want to log in?</h1>
            <p className="mb-8 leading-relaxed text-gray-400">
              You will be redirect to another page to continue with the log in
            </p>
            <div className="flex justify-center">
              <button
                onClick={handleLogin}
                className=" text-palidcolor bg-cardscolor uppercase font-bold border-0 py-2 px-6 focus:outline-none hover:bg-cardshover rounded text-lg"
              >
                Log In
              </button>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default LogIn;
