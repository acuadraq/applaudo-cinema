import React, { useContext, useState } from 'react';
import { LazyLoadImage } from 'react-lazy-load-image-component';
import 'react-lazy-load-image-component/src/effects/blur.css';
import { Link } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.min.css';
import { getW500 } from '../../adapters/Image';
import { MyContext } from '../../context/context';
import addOrRemove from '../../helpers/favoritesHandlers';
import { IPostFavorite, IStoredAccount } from '../../interfaces/login';
import { IMovies, IOnlyMovies } from '../../interfaces/movies';
import { IOnlyTV, ITvShows } from '../../interfaces/tvshows';
import Modal from '../Modal/Modal';

type mediaProps = {
  media: IMovies | ITvShows | undefined;
  type: string;
  getFavorites: () => void;
};

const Media = ({ media, type, getFavorites }: mediaProps) => {
  const isLogged: boolean | IStoredAccount = useContext(MyContext);
  const { id: accountId, sessionId } = isLogged as IStoredAccount;
  const [open, setOpen] = useState<boolean>(false);
  const [currentId, setCurrentId] = useState<number>();

  const remove = async () => {
    setOpen(!open);
    const submitObject: IPostFavorite = {
      'media_type': type,
      'media_id': currentId,
      'favorite': false,
    };
    const result = await addOrRemove(submitObject, accountId, sessionId, 'remove');
    if (result?.success) getFavorites();
  };

  const openCloseModal = () => setOpen(!open);

  const removeFromFavorites = (id: number) => {
    setCurrentId(id);
    openCloseModal();
  };

  return (
    <>
      <div className="grid sm:grid-cols-2 lg:grid-cols-3 gap-8 mt-8">
        {media?.results.length === 0 && <p>There are no {type} added to your favorites</p>}
        {media?.results.map(item => (
          <div key={item.id}>
            <Link to={`/${type}/${item.id}`}>
              <div className="grid items-center">
                <LazyLoadImage
                  effect="blur"
                  width="500"
                  height="281"
                  className="rounded-tl-md rounded-tr-md hover:shadow-lg mx-auto"
                  src={
                    item.backdrop_path
                      ? getW500(item.backdrop_path)
                      : 'https://i.imgur.com/B8jgKhQ.png'
                  }
                />
              </div>
            </Link>
            <div className="bg-cardscolor rounded-bl-md rounded-br-md p-5 mt-2">
              <Link to={`/${type}/${item.id}`}>
                <h3 className="font-bold hover:text-hovertext">
                  {type === 'movie' ? (item as IOnlyMovies).title : (item as IOnlyTV).name}
                </h3>
              </Link>
              <div className="mt-4 flex justify-between">
                <span className="text-sm">
                  {type === 'movie'
                    ? (item as IOnlyMovies).release_date.replaceAll('-', '/')
                    : (item as IOnlyTV).first_air_date.replaceAll('-', '/')}
                </span>
                <button
                  aria-label={`removeFavorite-${item.id}`}
                  onClick={() => removeFromFavorites(item.id)}
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="h-6 w-6 text-buttonscolor transform ease-in duration-150 hover:scale-110"
                    viewBox="0 0 20 20"
                    fill="currentColor"
                  >
                    <path
                      fillRule="evenodd"
                      d="M3.172 5.172a4 4 0 015.656 0L10 6.343l1.172-1.171a4 4 0 115.656 5.656L10 17.657l-6.828-6.829a4 4 0 010-5.656z"
                      clipRule="evenodd"
                    />
                  </svg>
                </button>
              </div>
            </div>
          </div>
        ))}
      </div>
      <Modal openModal={open} openCloseModal={openCloseModal} remove={remove} />
      <ToastContainer
        position="top-center"
        autoClose={3000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
    </>
  );
};

export default Media;
