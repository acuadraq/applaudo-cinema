import React, { useContext, useEffect, useState } from 'react';
import { favoriteTv } from '../../adapters/Login';
import { MyContext } from '../../context/context';
import { IStoredAccount } from '../../interfaces/login';
import { ITvShows } from '../../interfaces/tvshows';
import Header from './Header';
import Error from '../Error/Error';
import routesNames from '../../routes/customRoutes';
import { Link } from 'react-router-dom';
import Media from './Media';

const FavoritesTV = () => {
  const isLogged = useContext(MyContext);
  const { id, sessionId } = isLogged as IStoredAccount;
  const [favTV, setFavTV] = useState<ITvShows>();
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<boolean>(false);

  const getFavoriteTV = async () => {
    try {
      const list: ITvShows = await favoriteTv(id, sessionId);
      setFavTV(list);
      setLoading(false);
    } catch (err) {
      setError(true);
      setLoading(false);
    }
  };

  useEffect(() => {
    getFavoriteTV();
  }, []);

  return (
    <>
      {error && <Error />}
      {!error && (
        <>
          <Header />
          {loading && <div className="animation__loading mt-5" aria-label="Loading" />}
          {!loading && (
            <div className="big__container text-palidcolor">
              <div className="flex items-center">
                <h2 className="text-2xl">My Favorites</h2>
                <Link to={routesNames.favorites} className="mx-5 hover:text-hovertext text-lg">
                  Movies
                </Link>
                <span className="text-lg font-semibold">
                  TV <hr />
                </span>
              </div>
              <Media media={favTV} type="tv" getFavorites={getFavoriteTV} />
            </div>
          )}
        </>
      )}
    </>
  );
};

export default FavoritesTV;
