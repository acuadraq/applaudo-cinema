import React, { useContext } from 'react';
import { LazyLoadImage } from 'react-lazy-load-image-component';
import 'react-lazy-load-image-component/src/effects/blur.css';
import { getAvatar } from '../../adapters/Image';
import { MyContext } from '../../context/context';
import { IStoredAccount } from '../../interfaces/login';

const Header = () => {
  const isLogged = useContext(MyContext);
  const { name, avatar, username } = isLogged as IStoredAccount;

  return (
    <section className="bg-bgcolor">
      <div className="xl:container px-4 sm:px-8 lg:px-16 xl:px-24 mx-auto py-5 text-palidcolor">
        <div className="flex flex-wrap justify-center md:justify-start items-center">
          <LazyLoadImage
            src={getAvatar(avatar)}
            className="rounded-full"
            width="150"
            height="150"
            effect="blur"
          />
          <div className="ml-6 text-center md:text-left mt-2 md:mt-0">
            <h1 className="font-bold text-xl md:text-2xl">{name}</h1>
            <h2 className="mt-2">{username}</h2>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Header;
