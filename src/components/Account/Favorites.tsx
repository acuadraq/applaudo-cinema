import React, { useContext, useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { favoriteMovies } from '../../adapters/Login';
import { MyContext } from '../../context/context';
import { IStoredAccount } from '../../interfaces/login';
import { IMovies } from '../../interfaces/movies';
import routesNames from '../../routes/customRoutes';
import Error from '../Error/Error';
import Header from './Header';
import Media from './Media';

const Favorites = () => {
  const isLogged = useContext(MyContext);
  const { id, sessionId } = isLogged as IStoredAccount;
  const [favMovies, setFavMovies] = useState<IMovies>();
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<boolean>(false);

  const getFavoriteMovies = async () => {
    try {
      const list: IMovies = await favoriteMovies(id, sessionId);
      setFavMovies(list);
      setLoading(false);
    } catch (err) {
      setError(true);
      setLoading(false);
    }
  };

  useEffect(() => {
    getFavoriteMovies();
  }, []);

  return (
    <>
      {error && <Error />}
      {!error && (
        <>
          <Header />
          {loading && <div className="animation__loading mt-5" aria-label="Loading" />}
          {!loading && (
            <div className="big__container text-palidcolor">
              <div className="flex items-center">
                <h2 className="text-2xl">My Favorites</h2>
                <span className="mx-5 text-lg font-semibold">
                  Movies <hr />
                </span>
                <Link to={routesNames.favoritesTv} className="hover:text-hovertext text-lg">
                  TV
                </Link>
              </div>
              <Media media={favMovies} type="movie" getFavorites={getFavoriteMovies} />
            </div>
          )}
        </>
      )}
    </>
  );
};

export default Favorites;
