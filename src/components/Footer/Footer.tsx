import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
import { MyContext } from '../../context/context';
import routesNames from '../../routes/customRoutes';

const Footer = () => {
  const isLogged = useContext(MyContext);

  return (
    <footer className="text-palidcolor bg-bgcolor">
      <div className="container px-5 py-10 mx-auto flex md:items-center lg:items-start md:flex-row md:flex-nowrap flex-wrap flex-col">
        <div className="w-64 flex-shrink-0 md:mx-0 mx-auto text-center md:text-left">
          <Link
            to={routesNames.home}
            className="flex title-font items-center md:justify-start justify-center font-bold"
          >
            <span className="ml-3 text-xl">Applaudo Cinema</span>
          </Link>
        </div>
        <div className="flex-grow flex flex-wrap justify-end md:pl-20 -mb-10 md:mt-0 mt-10 md:text-left text-center">
          <div className="lg:w-1/4 md:w-1/2 w-full px-4">
            <h2 className="title-font font-bold uppercase tracking-widest text-sm mb-3">
              Navigation
            </h2>
            <nav className="list-none mb-10">
              <li>
                <Link to={routesNames.movies} className="text-palidcolor hover:text-hovertext">
                  Movies
                </Link>
              </li>
              <li className="mt-3">
                <Link to={routesNames.shows} className="text-palidcolor hover:text-hovertext">
                  TV Shows
                </Link>
              </li>
            </nav>
          </div>
          <div className="lg:w-1/4 md:w-1/2 w-full px-4">
            <h2 className="title-font font-bold uppercase tracking-widest text-sm mb-3">Account</h2>
            <nav className="list-none mb-10">
              <li>
                {!isLogged && (
                  <Link to={routesNames.login} className="text-palidcolor hover:text-hovertext">
                    Log In
                  </Link>
                )}
                {isLogged && (
                  <>
                    <li>
                      <Link
                        to={routesNames.favorites}
                        className="text-palidcolor hover:text-hovertext"
                      >
                        Favorites Movies
                      </Link>
                    </li>
                    <li className="mt-3">
                      <Link
                        to={routesNames.favoritesTv}
                        className="text-palidcolor hover:text-hovertext"
                      >
                        Favorites TV
                      </Link>
                    </li>
                  </>
                )}
              </li>
            </nav>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
