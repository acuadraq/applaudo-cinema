import React from 'react';
import errorImg from '../../static/img/errorImg.svg';

const Error = () => {
  return (
    <div className="container mx-auto flex px-5 py-24 items-center justify-center flex-col">
      <img
        src={errorImg}
        alt="Error Image"
        className="lg:w-2/6 md:w-3/6 w-5/6 mb-10 object-cover object-center rounded"
      />

      <div className="text-center lg:w-2/3 w-full">
        <h1 className="text-palidcolor font-bold text-2xl">
          Sorry about this, but there was an error
        </h1>
        <h2 className="text-palidcolor text-lg mt-5">Try Again, or wait a few minutes</h2>
      </div>
    </div>
  );
};

export default Error;
