import React, { useEffect, useState } from 'react';
import { searchQuery } from '../../adapters/Fetch';
import { ISearch } from '../../interfaces/search';
import { getPosterImage } from '../../adapters/Image';
import Card from '../Card/Card';
import ReactPaginate from 'react-paginate';
import Error from '../Error/Error';

const Search = () => {
  const [searchResults, setSearchResults] = useState<ISearch>();
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<boolean>(false);
  const urlSearch = new URL(window.location.href);

  const getResults = async (page?: number) => {
    setLoading(true);
    try {
      const list: ISearch = await searchQuery(page, urlSearch.search.replace('?', '&'));
      setSearchResults(list);
      setLoading(false);
    } catch (err) {
      setError(true);
      setLoading(false);
    }
  };

  const changePage = (selected: number) => getResults(selected + 1);

  useEffect(() => {
    getResults();
  }, [urlSearch.search]);

  return (
    <div className="xl:container px-4 sm:px-8 lg:px-16 xl:px-24 mx-auto py-5 md:py-10">
      {error && <Error />}
      {searchResults && !error && (
        <>
          <h1 className="section__title">Search Results</h1>
          <div>
            {loading && <div className="animation__loading" aria-label="Loading" />}
            {!loading && (
              <div className="mt-5 flex flex-wrap justify-around">
                {searchResults?.results.length === 0 && (
                  <p className="text-palidcolor text-xl">There was not results found</p>
                )}
                {searchResults?.results.map(result => (
                  <div key={result.id} className="mx-2 mb-10 transform hover:-translate-y-2">
                    <Card
                      title={result.name ? result.name : result.title}
                      id={result.id}
                      imgUrl={
                        result.poster_path || result.profile_path
                          ? getPosterImage(
                              result.poster_path ? result.poster_path : result.profile_path,
                            )
                          : null
                      }
                      type={`/${result.media_type}`}
                      searchType={result.media_type}
                    />
                  </div>
                ))}
              </div>
            )}
            <ReactPaginate
              previousLabel={'<'}
              nextLabel={'>'}
              pageCount={searchResults.total_pages}
              pageRangeDisplayed={2}
              marginPagesDisplayed={1}
              onPageChange={e => changePage(e.selected)}
              containerClassName={'container__pagination'}
              pageClassName={'md:mx-5'}
              pageLinkClassName={'page__link'}
              activeLinkClassName={'bg-cardscolor font-bold'}
              previousLinkClassName={'previous__after'}
              nextLinkClassName={'previous__after'}
              disabledClassName={'prev__after--disabled'}
            />
          </div>
        </>
      )}
    </div>
  );
};

export default Search;
