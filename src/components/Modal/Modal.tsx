import React from 'react';

type modalProps = {
  openModal: boolean;
  openCloseModal: () => void;
  remove: () => void;
};

const Modal = ({ openModal, openCloseModal, remove }: modalProps) => {
  return (
    <div
      className={`${
        openModal ? 'fixed' : 'hidden'
      } inset-0 w-full h-full z-20 bg-black bg-opacity-50 duration-300 overflow-y-auto`}
    >
      <div className="relative sm:w-3/4 md:w-1/2 lg:w-1/4 mx-2 sm:mx-auto my-20 opacity-100">
        <div className="relative bg-bgcolor shadow-lg rounded-md text-palidcolor z-20">
          <header className="flex items-center justify-between px-5 py-2">
            <h2 className="font-semibold">Remove From Favorites</h2>
            <button onClick={openCloseModal} className="focus:outline-none p-2">
              <svg
                className="fill-current"
                xmlns="http://www.w3.org/2000/svg"
                width="18"
                height="18"
                viewBox="0 0 18 18"
              >
                <path d="M14.53 4.53l-1.06-1.06L9 7.94 4.53 3.47 3.47 4.53 7.94 9l-4.47 4.47 1.06 1.06L9 10.06l4.47 4.47 1.06-1.06L10.06 9z"></path>
              </svg>
            </button>
          </header>
          <hr />
          <main className="p-2 text-center">
            <p>Are you sure you want to delete it from your favorites?</p>
          </main>
          <footer className="flex justify-end p-4">
            <button
              onClick={openCloseModal}
              className="bg-cardscolor font-semibold text-palidcolor py-2 w-24 rounded-lg hover:bg-cardshover focus:outline-none focus:ring shadow-lg hover:shadow-none transition-all duration-300"
            >
              Cancel
            </button>
            <button
              onClick={remove}
              className="bg-buttonscolor font-semibold text-palidcolor px-4 py-2 ml-3 w-24 rounded-lg hover:bg-red-700 focus:outline-none focus:ring shadow-lg hover:shadow-none transition-all duration-300"
            >
              Delete
            </button>
          </footer>
        </div>
      </div>
    </div>
  );
};

export default Modal;
