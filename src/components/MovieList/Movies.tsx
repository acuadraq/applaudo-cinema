import React, { useState, useEffect } from 'react';
import { getDiscovered } from '../../adapters/MoviesFetch';
import { IMovies } from '../../interfaces/movies';
import ReactPaginate from 'react-paginate';
import { getPosterImage } from '../../adapters/Image';
import Card from '../Card/Card';
import Error from '../Error/Error';
import Filter from '../Filter/Filter';

const Movies = () => {
  const [movies, setMovies] = useState<IMovies>();
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<boolean>(false);
  const [query, setQuery] = useState<string>('');

  const getMovies = async (page?: number, queries?: string) => {
    setLoading(true);
    try {
      const list: IMovies = await getDiscovered(page, queries);
      setMovies(list);
      setLoading(false);
    } catch (err) {
      setError(true);
      setLoading(false);
    }
  };

  const changePage = (selected: number) => {
    getMovies(selected + 1, query);
    window.scrollTo(0, 0);
  };

  const changeQuery = (newQuery: string) => setQuery(newQuery);

  const activateError = () => setError(true);

  useEffect(() => {
    getMovies();
  }, []);
  return (
    <>
      {error && <Error />}
      {!movies && loading && <div className="animation__loading mt-5" aria-label="Loading" />}
      {movies && !error && (
        <div className="xl:container px-4 sm:px-8 lg:px-16 2xl:px-24 mx-auto py-5 md:py-10">
          <h1 className="section__title">Popular Movies</h1>
          <div className="grid grid-cols-1 md:grid-cols-3 xl:grid-cols-4 gap-5 mt-5">
            <div>
              <Filter
                getInfo={getMovies}
                changeQuery={changeQuery}
                activateError={activateError}
                type="movie"
              />
            </div>
            <div className="md:col-span-2 xl:col-span-3">
              {loading && <div className="animation__loading" />}
              {!loading && (
                <div className="flex flex-wrap justify-around md:grid md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-5">
                  {movies?.results.length === 0 && (
                    <p className="text-palidcolor text-lg">No Movies were found</p>
                  )}
                  {movies?.results.map(movie => (
                    <div key={movie.id} className="transform hover:-translate-y-2">
                      <Card
                        id={movie.id}
                        title={movie.title}
                        imgUrl={movie.poster_path ? getPosterImage(movie.poster_path) : null}
                        type="/movie"
                      />
                    </div>
                  ))}
                </div>
              )}

              <ReactPaginate
                previousLabel={'<'}
                nextLabel={'>'}
                pageCount={movies.total_pages}
                pageRangeDisplayed={2}
                marginPagesDisplayed={1}
                onPageChange={e => changePage(e.selected)}
                containerClassName={'container__pagination'}
                pageClassName={'md:mx-5'}
                pageLinkClassName={'page__link'}
                activeLinkClassName={'bg-cardscolor font-bold'}
                previousLinkClassName={'previous__after'}
                nextLinkClassName={'previous__after'}
                disabledClassName={'prev__after--disabled'}
              />
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default Movies;
