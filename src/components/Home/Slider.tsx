import React from 'react';
import { IOnlyMovies } from '../../interfaces/movies';
import { Swiper, SwiperSlide } from 'swiper/react';
import SwiperCore, { Autoplay } from 'swiper';
import { LazyLoadImage } from 'react-lazy-load-image-component';
import 'react-lazy-load-image-component/src/effects/blur.css';
import 'swiper/swiper-bundle.min.css';
import { getFullImage } from '../../adapters/Image';

SwiperCore.use([Autoplay]);
type sliderProps = {
  movies: IOnlyMovies[] | undefined;
};

const Slider = ({ movies }: sliderProps) => {
  return (
    <Swiper autoplay={{ delay: 5000 }}>
      {movies?.slice(0, 5).map(movie => (
        <SwiperSlide key={movie.id}>
          <div className="relative w-full h-auto xl:h-sliderHeight">
            <h1 className="mx-auto absolute text-lg md:text-3xl font-bold bottom-10 left-3 md:left-10 text-white">
              {movie.title}
            </h1>
            <LazyLoadImage
              effect="blur"
              className="slider__image w-full object-cover relative bg-center"
              src={getFullImage(movie.backdrop_path)}
              alt={movie.title}
            />
          </div>
        </SwiperSlide>
      ))}
    </Swiper>
  );
};

export default Slider;
