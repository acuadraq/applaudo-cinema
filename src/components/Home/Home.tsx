import React, { useEffect, useState } from 'react';
import { getNowPlaying, getPopular, getTopRated } from '../../adapters/MoviesFetch';
import { getOnAirShows } from '../../adapters/ShowFetch';
import { IMovies } from '../../interfaces/movies';
import { Link } from 'react-router-dom';
import Slider from './Slider';
import { LazyLoadImage } from 'react-lazy-load-image-component';
import 'react-lazy-load-image-component/src/effects/blur.css';
import { getPosterImage, getW500 } from '../../adapters/Image';
import Card from '../Card/Card';
import Error from '../Error/Error';
import { ITvShows } from '../../interfaces/tvshows';

const Home = () => {
  const [sliderMovies, setSliderMovies] = useState<IMovies>();
  const [popularMovies, setPopularMovies] = useState<IMovies>();
  const [tvShows, setTvShows] = useState<ITvShows>();
  const [topMovies, setTopMovies] = useState<IMovies>();
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<boolean>(false);

  const getTvShows = async () => {
    try {
      const list = await getOnAirShows();
      setTvShows(list);
      setLoading(false);
    } catch (err) {
      setError(true);
      setLoading(false);
    }
  };

  const getTopRatedMovies = async () => {
    try {
      const list = await getTopRated();
      setTopMovies(list);
      getTvShows();
    } catch (err) {
      setError(true);
      setLoading(false);
    }
  };

  const getPopularMovies = async () => {
    try {
      const list = await getPopular();
      setPopularMovies(list);
      getTopRatedMovies();
    } catch (err) {
      setError(true);
      setLoading(false);
    }
  };

  const getSliderMovies = async () => {
    try {
      const list = await getNowPlaying();
      setSliderMovies(list);
      getPopularMovies();
    } catch (err) {
      setError(true);
      setLoading(false);
    }
  };

  useEffect(() => {
    getSliderMovies();
  }, []);

  return (
    <>
      {loading && <div className="animation__loading mt-5" aria-label="Loading" />}
      {error && <Error />}
      {!loading && !error && (
        <>
          {sliderMovies && <Slider movies={sliderMovies.results} />}
          <div className="container px-4 sm:px-8 lg:px-16 xl:px-24 mx-auto py-5 md:py-10">
            <section>
              <h2 className="section__title">Movies Trending Now</h2>
              <div className="scroll__div">
                {popularMovies?.results.map(popular => (
                  <div key={popular.id} className="scroll-div__child">
                    <Card
                      id={popular.id}
                      title={popular.title}
                      imgUrl={getPosterImage(popular.poster_path)}
                      type="movie"
                    />
                  </div>
                ))}
              </div>
            </section>
            <section className="mt-10">
              <h2 className="section__title">Our Top Rated Movies</h2>
              <div className="scroll__div">
                {topMovies?.results.slice(0, 10).map(movie => (
                  <div key={movie.id} className="inline-block m-5">
                    <div className="mb-3 image-container">
                      <Link to={`/movie/${movie.id}`} className="mb-3">
                        <LazyLoadImage
                          className="h-cardsheight w-96 lg:w-full object-cover bg-center"
                          src={getW500(movie.backdrop_path)}
                          alt={movie.title}
                          effect="blur"
                        />
                      </Link>
                    </div>
                    <div>
                      <Link to={`/movie/${movie.id}`}>
                        <h3 className="text-palidcolor font-bold hover:text-hovertext">
                          {movie.title}
                        </h3>
                      </Link>
                    </div>
                  </div>
                ))}
              </div>
            </section>
            <section className="mt-10">
              <h2 className="section__title">TV Shows on the air</h2>
              <div className="scroll__div">
                {tvShows?.results.map(show => (
                  <div key={show.id} className="scroll-div__child">
                    <Card
                      id={show.id}
                      title={show.name}
                      imgUrl={getPosterImage(show.poster_path)}
                      type="tv"
                    />
                  </div>
                ))}
              </div>
            </section>
          </div>
        </>
      )}
    </>
  );
};

export default Home;
