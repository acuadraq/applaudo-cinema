const routesNames = {
  home: '/',
  movies: '/movie',
  movieDetail: '/movie/:id',
  movieCast: '/movie/:id/cast',
  shows: '/tv',
  showDetail: '/tv/:id',
  tvCast: '/tv/:id/cast',
  seasons: '/tv/:id/seasons',
  episodes: '/tv/:id/seasons/:seasonNumber',
  search: '/search',
  person: '/person/:id',
  login: '/login',
  approved: '/approved',
  favorites: '/favorites',
  favoritesTv: '/favorites/tv',
};

export default routesNames;
