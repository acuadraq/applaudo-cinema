import React from 'react';
import { Navigate, Outlet } from 'react-router';
import Footer from '../components/Footer/Footer';
import Navbar from '../components/Navbar/Navbar';
import { MyContext } from '../context/context';
import useStorage from '../customhooks/useStorage';
import routesNames from './customRoutes';

const PrivateRoute = () => {
  const [isLogged] = useStorage('sessionId', false);

  return (
    <>
      {!isLogged ? (
        <Navigate to={routesNames.home} />
      ) : (
        <>
          <MyContext.Provider value={isLogged}>
            <Navbar />
            <div className="min-h-screen">
              <Outlet />
            </div>
            <Footer />
          </MyContext.Provider>
        </>
      )}
    </>
  );
};

export default PrivateRoute;
