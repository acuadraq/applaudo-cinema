import React from 'react';
import { Outlet } from 'react-router';
import Footer from '../components/Footer/Footer';
import Navbar from '../components/Navbar/Navbar';
import { MyContext } from '../context/context';
import useLocalStorage from '../customhooks/useStorage';

const MainRoute = () => {
  const [isLog] = useLocalStorage('sessionId', false);
  return (
    <>
      <MyContext.Provider value={isLog}>
        <Navbar />
        <div className="min-h-screen">
          <Outlet />
        </div>
        <Footer />
      </MyContext.Provider>
    </>
  );
};

export default MainRoute;
