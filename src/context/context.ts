import { createContext } from 'react';
import { IStoredAccount } from '../interfaces/login';

export const MyContext = createContext<boolean | IStoredAccount>(false);
