export interface ICertification {
  certifications: {
    US: IOnlyCertification[];
    CA: IOnlyCertification[];
    DE: IOnlyCertification[];
    GB: IOnlyCertification[];
    AU: IOnlyCertification[];
    BR: IOnlyCertification[];
    FR: IOnlyCertification[];
    NZ: IOnlyCertification[];
    IN: IOnlyCertification[];
  };
}

export interface IOnlyCertification {
  certification: string;
  meaning: string;
  order: number;
}
