export interface ICast {
  id: number;
  cast: ICasts[];
  crew: ICrew[];
}

export interface ICasts {
  adult: boolean;
  gender: number | null;
  id: number;
  'known_for_department': string;
  name: string;
  'original_name': string;
  popularity: number;
  'profile_path': string | null;
  'cast_id': number;
  character: string;
  'credit_id': string;
  order: number;
}

export interface ICrew {
  adult: boolean;
  gender: number | null;
  id: number;
  'known_for_department': string;
  name: string;
  'original_name': string;
  popularity: number;
  'profile_path': string | null;
  'credit_id': string;
  department: string;
  job: string;
}
