export interface INewToken {
  success: boolean;
  'expires_at': string;
  'request_token': string;
}

export interface ISessionPost {
  'request_token': string | null;
}

export interface ISessionID {
  'session_id': string | null;
}

export interface ILogout {
  success: boolean;
}

export interface ISession {
  success: boolean;
  'session_id': string;
}

export interface IAccount {
  avatar: {
    gravatar: {
      hash: string;
    };
    tmdb: {
      'avatar_path': string;
    };
  };
  id: number;
  'iso_639_1': string;
  'iso_3166_1': string;
  name: string;
  'include_adult': boolean;
  username: string;
}

export interface IStoredAccount {
  id: number;
  name: string;
  username: string;
  avatar: string;
  sessionId: string;
}

export interface ISuccessFavorite {
  'status_code': number;
  'status_message': string;
  'success': boolean;
}

export interface IPostFavorite {
  'media_type': string;
  'media_id': number | undefined;
  favorite: boolean;
}

export interface IIsInFavorites {
  id: number;
  favorite: boolean;
  rated:
    | {
        value: number;
      }
    | boolean;
  watchlist: boolean;
}
