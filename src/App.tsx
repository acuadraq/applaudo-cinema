import React, { useEffect } from 'react';
import { useLocation } from 'react-router-dom';
import { useRoutes } from 'react-router-dom';
import Home from './components/Home/Home';
import Movies from './components/MovieList/Movies';
import Search from './components/Search/Search';
import Shows from './components/ShowsList/Shows';
import routesNames from './routes/customRoutes';
import NotFound from './components/NotFound/NotFound';
import MainRoute from './routes/MainRoute';
import MovieDetail from './components/MovieDetail/MovieDetail';
import TvDetail from './components/TVDetail/TvDetail';
import Seasons from './components/TVDetail/Seasons';
import Episodes from './components/TVDetail/Episodes';
import MovieCast from './components/MovieDetail/MovieCast';
import TVCast from './components/TVDetail/TVCast';
import Person from './components/PersonDetail/Person';
import LogIn from './components/LogIn/LogIn';
import Approved from './components/LogIn/Approved';
import Favorites from './components/Account/Favorites';
import PublicRoute from './routes/PublicRoute';
import PrivateRoute from './routes/PrivateRoute';
import FavoritesTV from './components/Account/FavoritesTV';

const App = () => {
  const location = useLocation();

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [location]);

  const mainRoutes = {
    path: routesNames.home,
    element: <MainRoute />,
    children: [
      { path: '*', element: <NotFound /> },
      { path: routesNames.home, element: <Home /> },
      { path: routesNames.movies, element: <Movies /> },
      { path: routesNames.movieDetail, element: <MovieDetail /> },
      { path: routesNames.movieCast, element: <MovieCast /> },
      { path: routesNames.shows, element: <Shows /> },
      { path: routesNames.showDetail, element: <TvDetail /> },
      { path: routesNames.tvCast, element: <TVCast /> },
      { path: routesNames.seasons, element: <Seasons /> },
      { path: routesNames.episodes, element: <Episodes /> },
      { path: routesNames.search, element: <Search /> },
      { path: routesNames.person, element: <Person /> },
    ],
  };

  const publicRoutes = {
    path: routesNames.home,
    element: <PublicRoute />,
    children: [
      { path: routesNames.login, element: <LogIn /> },
      { path: routesNames.approved, element: <Approved /> },
    ],
  };

  const privateRoutes = {
    path: routesNames.home,
    element: <PrivateRoute />,
    children: [
      { path: routesNames.favorites, element: <Favorites /> },
      { path: routesNames.favoritesTv, element: <FavoritesTV /> },
    ],
  };

  const routing = useRoutes([mainRoutes, publicRoutes, privateRoutes]);

  return <>{routing}</>;
};

export default App;
