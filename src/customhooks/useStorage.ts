import { useState, useEffect } from 'react';
import { IStoredAccount } from '../interfaces/login';

const useLocalStorage = (key: string, defaultValue: boolean | IStoredAccount) => {
  const stored = localStorage.getItem(key);
  const initial = stored ? JSON.parse(stored) : defaultValue;
  const [value, setValue] = useState(initial);

  useEffect(() => {
    localStorage.setItem(key, JSON.stringify(value));
  }, [key, value]);

  return [value, setValue];
};
export default useLocalStorage;
