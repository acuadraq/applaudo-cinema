import React from 'react';
import { FunctionComponent, ReactElement, ReactNode } from 'react';
import { render } from '@testing-library/react';
import { BrowserRouter, useLocation } from 'react-router-dom';
import { MyContext } from '../context/context';

type PropT = {
  children: ReactNode;
};

const LocationDisplay = () => {
  const location = useLocation();
  return <div data-testid="location-display">{location.pathname}</div>;
};

const Wrapper = ({ children }: PropT) => {
  return (
    <MyContext.Provider value={false}>
      <BrowserRouter>
        {children}
        <LocationDisplay />
      </BrowserRouter>
    </MyContext.Provider>
  );
};

const renderWithRouter = (ui: ReactElement, route: string = '/') => {
  window.history.pushState({}, 'Test Page', route);
  return render(ui, { wrapper: Wrapper as FunctionComponent });
};

export default renderWithRouter;
