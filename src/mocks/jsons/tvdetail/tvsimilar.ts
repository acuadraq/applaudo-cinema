/* eslint-disable quotes */
export const mockTvSimilar = {
  'page': 1,
  'results': [
    {
      'adult': false,
      'backdrop_path': '/1KclsHGiGNTkn11puPbMMnnpRRT.jpg',
      'genre_ids': [80, 9648, 10759, 18],
      'id': 2593,
      'name': 'Without a Trace',
      'origin_country': ['US'],
      'original_language': 'en',
      'original_name': 'Without a Trace',
      'overview':
        'The series follows the ventures of a Missing Persons Unit of the FBI in New York City.',
      'popularity': 64.703,
      'poster_path': '/iNhE283iY7xtS8zCjhSxpTfOzn0.jpg',
      'first_air_date': '2002-09-26',
      'vote_average': 7.2,
      'vote_count': 170,
    },
    {
      'adult': false,
      'backdrop_path': '/uro2Khv7JxlzXtLb8tCIbRhkb9E.jpg',
      'genre_ids': [10759, 18, 10765],
      'id': 1402,
      'name': 'The Walking Dead',
      'origin_country': ['US'],
      'original_language': 'en',
      'original_name': 'The Walking Dead',
      'overview':
        "Sheriff's deputy Rick Grimes awakens from a coma to find a post-apocalyptic world dominated by flesh-eating zombies. He sets out to find his family and encounters many other survivors along the way.",
      'popularity': 600.637,
      'poster_path': '/w21lgYIi9GeUH5dO8l3B9ARZbCB.jpg',
      'first_air_date': '2010-10-31',
      'vote_average': 8.093,
      'vote_count': 12106,
    },
    {
      'adult': false,
      'backdrop_path': '/80BRASQnT9KT7BkFeEI0EdeRIF3.jpg',
      'genre_ids': [18, 10765, 10759],
      'id': 1403,
      'name': "Marvel's Agents of S.H.I.E.L.D.",
      'origin_country': ['US'],
      'original_language': 'en',
      'original_name': "Marvel's Agents of S.H.I.E.L.D.",
      'overview':
        'Agent Phil Coulson of S.H.I.E.L.D. (Strategic Homeland Intervention, Enforcement and Logistics Division) puts together a team of agents to investigate the new, the strange and the unknown around the globe, protecting the ordinary from the extraordinary.',
      'popularity': 115.483,
      'poster_path': '/gHUCCMy1vvj58tzE3dZqeC9SXus.jpg',
      'first_air_date': '2013-09-24',
      'vote_average': 7.5,
      'vote_count': 2706,
    },
    {
      'adult': false,
      'backdrop_path': '/vNnLAKmoczRlNarxyGrrw0KSOeX.jpg',
      'genre_ids': [80, 18, 9648, 10759],
      'id': 1412,
      'name': 'Arrow',
      'origin_country': ['US'],
      'original_language': 'en',
      'original_name': 'Arrow',
      'overview':
        'Spoiled billionaire playboy Oliver Queen is missing and presumed dead when his yacht is lost at sea. He returns five years later a changed man, determined to clean up the city as a hooded vigilante armed with a bow.',
      'popularity': 170.234,
      'poster_path': '/gKG5QGz5Ngf8fgWpBsWtlg5L2SF.jpg',
      'first_air_date': '2012-10-10',
      'vote_average': 6.705,
      'vote_count': 4797,
    },
    {
      'adult': false,
      'backdrop_path': '/7sJrNKwzyJWnFPFpDL9wnZ859LZ.jpg',
      'genre_ids': [18, 9648, 80],
      'id': 1415,
      'name': 'Elementary',
      'origin_country': ['US'],
      'original_language': 'en',
      'original_name': 'Elementary',
      'overview':
        "A modern-day drama about a crime-solving duo that cracks the NYPD's most impossible cases. Following his fall from grace in London and a stint in rehab, eccentric Sherlock escapes to Manhattan where his wealthy father forces him to live with his worst nightmare - a sober companion, Dr. Watson.",
      'popularity': 75.079,
      'poster_path': '/q9dObe29W4bDpgzUfOOH3ZnzDbR.jpg',
      'first_air_date': '2012-09-27',
      'vote_average': 7.503,
      'vote_count': 1200,
    },
    {
      'adult': false,
      'backdrop_path': '/zOnLMRD3PIhwO5KZmWMVEnLokMo.jpg',
      'genre_ids': [18, 80],
      'id': 1419,
      'name': 'Castle',
      'origin_country': ['US'],
      'original_language': 'en',
      'original_name': 'Castle',
      'overview':
        'After a serial killer imitates the plots of his novels, successful mystery novelist Richard "Rick" Castle receives permission from the Mayor of New York City to tag along with an NYPD homicide investigation team for research purposes.',
      'popularity': 167.629,
      'poster_path': '/diXBeMzvfJb2iJg3G0kCUaMCzEc.jpg',
      'first_air_date': '2009-03-09',
      'vote_average': 7.989,
      'vote_count': 1235,
    },
    {
      'adult': false,
      'backdrop_path': '/nVKaObLEsiPyqfmrPXW4BW1MT3n.jpg',
      'genre_ids': [10762, 16, 10759],
      'id': 1482,
      'name': 'Spider-Man',
      'origin_country': ['US', 'CA'],
      'original_language': 'en',
      'original_name': 'Spider-Man',
      'overview':
        'Spider-Man is an animated television series that ran from September 9, 1967 to June 14, 1970. It was jointly produced in Canada and the United States and was the first animated adaptation of the Spider-Man comic book series, created by writer Stan Lee and artist Steve Ditko. It first aired on the ABC television network in the United States but went into syndication at the start of the third season. Grantray-Lawrence Animation produced the first season. Seasons 2 and 3 were crafted by producer Ralph Bakshi in New York City. In Canada, it is currently airing on Teletoon Retro. An internet meme, commonly known as 1960s Spiderman, regarding the series has received an overwhelming amount of popularity. The meme consists of a screenshot taken at a random part of the series and adding an inappropriate and/or witty text. Since the death of Max Ferguson on March 7 2013, there are only three surviving members from the cast. Those three being Paul Soles the voice of Spider-Man, Chris Wiggins the voice of Mysterio and Carl Banas the voice of the Scorpion.',
      'popularity': 25.631,
      'poster_path': '/n6eMF2lPQxiGmh2D612R5Tuxbzm.jpg',
      'first_air_date': '1967-09-09',
      'vote_average': 7.647,
      'vote_count': 126,
    },
    {
      'adult': false,
      'backdrop_path': '/dL89XHH0VcgXAeCzWreyz8SRaYO.jpg',
      'genre_ids': [10759, 16, 10765],
      'id': 1487,
      'name': 'Static Shock',
      'origin_country': ['US'],
      'original_language': 'en',
      'original_name': 'Static Shock',
      'overview': 'The adventures of a teenage superhero who fights crime in Dakota City.',
      'popularity': 19.023,
      'poster_path': '/dr6sL7qwTUtETetetVHstAugHFz.jpg',
      'first_air_date': '2000-09-23',
      'vote_average': 8.056,
      'vote_count': 144,
    },
    {
      'adult': false,
      'backdrop_path': '/1ZVXsbWyFCXdxk3heDrfl5iYBMu.jpg',
      'genre_ids': [10759, 16, 10765],
      'id': 1618,
      'name': 'Justice League',
      'origin_country': ['US'],
      'original_language': 'en',
      'original_name': 'Justice League',
      'overview':
        'The long-awaited rebirth of the greatest superhero team of all time: Batman, Superman, The Flash, Wonder Woman, Hawkgirl, Green Lantern and Martian Manhunter.',
      'popularity': 14.384,
      'poster_path': '/nI5IifJjjlQGNY7Zcf1f9YAu6RM.jpg',
      'first_air_date': '2001-11-17',
      'vote_average': 8.096,
      'vote_count': 272,
    },
    {
      'adult': false,
      'backdrop_path': '/nVHovBi2yqZAYW17273ytA8SE7c.jpg',
      'genre_ids': [16, 10759],
      'id': 1664,
      'name': 'Spider-Man: The New Animated Series',
      'origin_country': ['US', 'CA'],
      'original_language': 'en',
      'original_name': 'Spider-Man: The New Animated Series',
      'overview':
        "Spider-Man: The New Animated Series is an American animated series based on the Marvel comic book superhero character Spider-Man, which ran for one season, 13 episodes, starting on July 11, 2003. It is a loose continuation of 2002's Spider-Man film directed by Sam Raimi. The show was made using computer generated imagery rendered in cel shading and was broadcast on MTV, and YTV. Eight months later after the series finale, episodes aired in reruns on ABC Family as part of the Jetix television programming block. The series featured a far more mature version of the character than typically seen on television for any animated comic book adaptation. Throughout the series, characters are clearly killed, rather than the usual ambiguous disappearance, and several characters are strongly implied to have had sex.",
      'popularity': 43.771,
      'poster_path': '/z4CRC612QrGrC1kLtDm8ykLeIrs.jpg',
      'first_air_date': '2003-07-11',
      'vote_average': 7.233,
      'vote_count': 247,
    },
    {
      'adult': false,
      'backdrop_path': '/l0qVZIpXtIo7km9u5Yqh0nKPOr5.jpg',
      'genre_ids': [35, 18],
      'id': 1668,
      'name': 'Friends',
      'origin_country': ['US'],
      'original_language': 'en',
      'original_name': 'Friends',
      'overview':
        'Friends is an American television sitcom created by David Crane and Marta Kauffman, which aired on NBC from September 22, 1994, to May 6, 2004, lasting ten seasons. With an ensemble cast starring Jennifer Aniston, Courteney Cox, Lisa Kudrow, Matt LeBlanc, Matthew Perry and David Schwimmer, the show revolves around six friends in their 20s and 30s who live in Manhattan, New York City. The series was produced by Bright/Kauffman/Crane Productions, in association with Warner Bros. Television. The original executive producers were Kevin S. Bright, Kauffman, and Crane.',
      'popularity': 169.071,
      'poster_path': '/f496cm9enuEsZkSPzCwnTESEK5s.jpg',
      'first_air_date': '1994-09-22',
      'vote_average': 8.448,
      'vote_count': 5229,
    },
    {
      'adult': false,
      'backdrop_path': '/jAgxwUKNBD0AwvTQHmVGyPGuvNB.jpg',
      'genre_ids': [35],
      'id': 2730,
      'name': 'I Love Lucy',
      'origin_country': ['US'],
      'original_language': 'en',
      'original_name': 'I Love Lucy',
      'overview':
        'Cuban Bandleader Ricky Ricardo would be happy if his wife Lucy would just be a housewife. Instead she tries constantly to perform at the Tropicana where he works, and make life comically frantic in the apartment building they share with landlords Fred and Ethel Mertz, who also happen to be their best friends.',
      'popularity': 15.222,
      'poster_path': '/mPUSrFTvQqwEqTtfukxJBtkbxWj.jpg',
      'first_air_date': '1951-10-15',
      'vote_average': 8.142,
      'vote_count': 144,
    },
    {
      'adult': false,
      'backdrop_path': '/ikG6bO8Mw25neDRBQqS6QY9PUEt.jpg',
      'genre_ids': [35],
      'id': 2742,
      'name': 'Flight of the Conchords',
      'origin_country': ['US'],
      'original_language': 'en',
      'original_name': 'Flight of the Conchords',
      'overview':
        'The trials and tribulations of a two man, digi-folk band who have moved from New Zealand to New York in the hope of forging a successful music career. So far they\'ve managed to find a manager (whose "other" job is at the New Zealand Consulate), one fan (a married obsessive) and one friend (who owns the local pawn shop) -- but not much else.',
      'popularity': 14.949,
      'poster_path': '/ynboK8qiOTz0X44r59LpfF8jBP5.jpg',
      'first_air_date': '2007-06-17',
      'vote_average': 7.836,
      'vote_count': 219,
    },
    {
      'adult': false,
      'backdrop_path': '/x6rj0QZgLnWwIoJDDTyEg2TCIv1.jpg',
      'genre_ids': [35, 18],
      'id': 2251,
      'name': 'Taxi',
      'origin_country': ['US'],
      'original_language': 'en',
      'original_name': 'Taxi',
      'overview':
        'Louie De Palma is a cantankerous, acerbic taxi dispatcher in New York City. He tries to maintain order over a collection of varied and strange characters who drive for him. As he bullies and insults them from the safety of his “cage,” they form a special bond among themselves, becoming friends and supporting each other through the inevitable trials and tribulations of life.',
      'popularity': 20.918,
      'poster_path': '/pua3A2XSaO6bP6WhVDBRbcyRTAS.jpg',
      'first_air_date': '1978-09-12',
      'vote_average': 7.213,
      'vote_count': 94,
    },
    {
      'adult': false,
      'backdrop_path': '/xGjg5J21pmMabRT4aDd9hyts4kP.jpg',
      'genre_ids': [10765, 35, 10759],
      'id': 2287,
      'name': 'Batman',
      'origin_country': ['US'],
      'original_language': 'en',
      'original_name': 'Batman',
      'overview':
        'Wealthy entrepreneur Bruce Wayne and his ward Dick Grayson lead a double life: they are actually crime fighting duo Batman and Robin. A secret Batpole in the Wayne mansion leads to the Batcave, where Police Commissioner Gordon often calls with the latest emergency threatening Gotham City. Racing the the scene of the crime in the Batmobile, Batman and Robin must (with the help of their trusty Bat-utility-belt) thwart the efforts of a variety of master criminals, including The Riddler, The Joker, Catwoman, and The Penguin.',
      'popularity': 37.453,
      'poster_path': '/1ZEJuuDh0Zpi5ELM3Zev0GBhQ3R.jpg',
      'first_air_date': '1966-01-12',
      'vote_average': 7.464,
      'vote_count': 345,
    },
    {
      'adult': false,
      'backdrop_path': '/lt6sueOPDt721p9nqtpXKa3VZGu.jpg',
      'genre_ids': [18, 10759],
      'id': 3572,
      'name': 'Kojak',
      'origin_country': ['US'],
      'original_language': 'en',
      'original_name': 'Kojak',
      'overview': 'Detective Lieutenant Theo Kojak investigates crimes in New York City.',
      'popularity': 13.663,
      'poster_path': '/gt0h2VxuDRucP1Y7o9nptdixMCZ.jpg',
      'first_air_date': '1973-10-24',
      'vote_average': 6.992,
      'vote_count': 61,
    },
    {
      'adult': false,
      'backdrop_path': '/3SGcL8QL7ZASCBDlNZWw8OdIcNm.jpg',
      'genre_ids': [10759, 16, 18, 9648],
      'id': 2098,
      'name': 'Batman: The Animated Series',
      'origin_country': ['US'],
      'original_language': 'en',
      'original_name': 'Batman: The Animated Series',
      'overview':
        'Vowing to avenge the murder of his parents, Bruce Wayne devotes his life to wiping out crime in Gotham City as the masked vigilante "Batman".',
      'popularity': 55.948,
      'poster_path': '/lBomQFW1vlm1yUYMNSbFZ45R4Ox.jpg',
      'first_air_date': '1992-09-05',
      'vote_average': 8.415,
      'vote_count': 1059,
    },
    {
      'adult': false,
      'backdrop_path': '/cIWdLPKkIe07zgrBJLsPIpOKill.jpg',
      'genre_ids': [35],
      'id': 2144,
      'name': 'The Lucy Show',
      'origin_country': ['US'],
      'original_language': 'en',
      'original_name': 'The Lucy Show',
      'overview':
        "The Lucy Show is an American sitcom that aired on CBS from 1962–68. It was Lucille Ball's follow-up to I Love Lucy. A significant change in cast and premise for the 1965–66 season divides the program into two distinct eras; aside from Ball, only Gale Gordon, who joined the program for its second season, remained. For the first three seasons, Vivian Vance was the co-star.\n\nThe earliest scripts were entitled The Lucille Ball Show, but when this title was declined, producers thought of calling the show This Is Lucy or The New Adventures of Lucy, before deciding on the title The Lucy Show. Ball won consecutive Emmy Awards as Outstanding Lead Actress in a Comedy Series for the series' final two seasons, 1966–67 and 1967–68.",
      'popularity': 12.529,
      'poster_path': '/sdmFoI7pKzE4XoIVrVNt4ouXlbH.jpg',
      'first_air_date': '1962-10-01',
      'vote_average': 7.54,
      'vote_count': 25,
    },
    {
      'adult': false,
      'backdrop_path': '/mWy8RbyTWASiz2irDESGILVkGJ4.jpg',
      'genre_ids': [18, 10765],
      'id': 32729,
      'name': 'No Ordinary Family',
      'origin_country': ['US'],
      'original_language': 'en',
      'original_name': 'No Ordinary Family',
      'overview':
        'After their plane crashes into the Amazon River, each member of the Powell family starts to show signs of new, unique and distinct super powers.',
      'popularity': 16.419,
      'poster_path': '/2Hytyjin1oa8kwpPDTpRiEQvXpL.jpg',
      'first_air_date': '2010-09-28',
      'vote_average': 6.674,
      'vote_count': 144,
    },
    {
      'adult': false,
      'backdrop_path': '/o1SmFTCN6HA8dGcLqX6SyQFhiqV.jpg',
      'genre_ids': [35, 18],
      'id': 32962,
      'name': 'Louie',
      'origin_country': ['US'],
      'original_language': 'en',
      'original_name': 'Louie',
      'overview':
        'Louis C.K. stars as a fictionalized version of himself; a comedian and newly divorced father raising his two daughters in New York City.',
      'popularity': 16.68,
      'poster_path': '/ubWj0BLRV1UDP8ELnTr2is2Wl0Y.jpg',
      'first_air_date': '2010-06-29',
      'vote_average': 7.678,
      'vote_count': 335,
    },
  ],
  'total_pages': 500,
  'total_results': 10000,
};
