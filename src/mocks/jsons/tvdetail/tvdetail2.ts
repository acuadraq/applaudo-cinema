export const mockTVdetail2 = {
  'backdrop_path': '/bfXTYu3oLtEUL0jAxgx7Jy56yHl.jpg',
  'created_by': [
    {
      'id': 939147,
      'credit_id': '5f217b49c048a900372f0583',
      'name': 'Yeon Sang-ho',
      'gender': 2,
      'profile_path': '/dLP1p4AkU1zwJkDboct58Uub9S7.jpg',
    },
    {
      'id': 2725917,
      'credit_id': '61588a4ff1759c0044c79781',
      'name': 'Choi Gyu-seok',
      'gender': 0,
      'profile_path': null,
    },
  ],
  'episode_run_time': [52],
  'first_air_date': '2021-11-19',
  'genres': [
    {
      'id': 18,
      'name': 'Drama',
    },
    {
      'id': 80,
      'name': 'Crime',
    },
    {
      'id': 9648,
      'name': 'Mystery',
    },
    {
      'id': 10765,
      'name': 'Sci-Fi & Fantasy',
    },
  ],
  'homepage': 'https://www.netflix.com/title/81256675',
  'id': 106651,
  'in_production': false,
  'languages': ['ko'],
  'last_air_date': '2021-11-19',
  'last_episode_to_air': {
    'air_date': '2021-11-19',
    'episode_number': 6,
    'id': 2823346,
    'name': '',
    'overview':
      'With detrimental information hanging over its head, The New Truth scrambles to protect its power by preventing the public from learning the truth.',
    'production_code': '',
    'season_number': 1,
    'still_path': '/oGdlZt118qzw1Iar5YH3Ddy4jCX.jpg',
    'vote_average': 9.5,
    'vote_count': 2,
  },
  'name': 'Hellbound',
  'next_episode_to_air': null,
  'networks': [
    {
      'name': 'Netflix',
      'id': 213,
      'logo_path': '/wwemzKWzjKYJFfCeiB57q3r4Bcm.png',
      'origin_country': '',
    },
  ],
  'number_of_episodes': 6,
  'number_of_seasons': 1,
  'origin_country': ['KR'],
  'original_language': 'ko',
  'original_name': '지옥',
  'overview':
    'Unearthly beings deliver bloody condemnations, sending individuals to hell and giving rise to a religious group founded on the idea of divine justice.',
  'popularity': 1483.001,
  'poster_path': '/5NYdSAnDVIXePrSG2dznHdiibMk.jpg',
  'production_companies': [
    {
      'id': 127541,
      'logo_path': '/AnQAgE7H8FWpDUQO0BCAaAOcVqg.png',
      'name': 'Climax Studios',
      'origin_country': 'KR',
    },
  ],
  'production_countries': [
    {
      'iso_3166_1': 'KR',
      'name': 'South Korea',
    },
  ],
  'seasons': [
    {
      'air_date': '2021-11-19',
      'episode_count': 6,
      'id': 157614,
      'name': 'Season 1',
      'overview': '',
      'poster_path': '/5NYdSAnDVIXePrSG2dznHdiibMk.jpg',
      'season_number': 1,
    },
  ],
  'spoken_languages': [
    {
      'english_name': 'Korean',
      'iso_639_1': 'ko',
      'name': '한국어/조선말',
    },
  ],
  'status': 'Ended',
  'tagline': 'Your time is up.',
  'type': 'Scripted',
  'vote_average': 7.8,
  'vote_count': 245,
};
