export const mockMovieCast = {
  'id': 568124,
  'cast': [
    {
      'adult': false,
      'gender': 1,
      'id': 968367,
      'known_for_department': 'Acting',
      'name': 'Stephanie Beatriz',
      'original_name': 'Stephanie Beatriz',
      'popularity': 7.272,
      'profile_path': '/pGo7zMGsMXOMSMZc68Xi3LvzeP0.jpg',
      'cast_id': 8,
      'character': 'Mirabel Madrigal (voice)',
      'credit_id': '5fee8b5e176a94003fe4998a',
      'order': 0,
    },
    {
      'adult': false,
      'gender': 2,
      'id': 5723,
      'known_for_department': 'Acting',
      'name': 'John Leguizamo',
      'original_name': 'John Leguizamo',
      'popularity': 5.403,
      'profile_path': '/2P0LTIKMUZn7BG1q9S9e5FZFZkn.jpg',
      'cast_id': 29,
      'character': 'Bruno Madrigal (voice)',
      'credit_id': '618a06d8fb5299008d028b56',
      'order': 1,
    },
    {
      'adult': false,
      'gender': 1,
      'id': 2521631,
      'known_for_department': 'Acting',
      'name': 'María Cecilia Botero',
      'original_name': 'María Cecilia Botero',
      'popularity': 1.749,
      'profile_path': '/nH6eBBgv1PI9J6Ftkn01ny1TMt1.jpg',
      'cast_id': 17,
      'character': 'Abuela Alma Madrigal (voice)',
      'credit_id': '60e6fe3e8de0ae005d50fa10',
      'order': 2,
    },
    {
      'adult': false,
      'gender': 2,
      'id': 18975,
      'known_for_department': 'Acting',
      'name': 'Wilmer Valderrama',
      'original_name': 'Wilmer Valderrama',
      'popularity': 4.612,
      'profile_path': '/ApA03vxxobrGauXmQ3LpGM5EKjD.jpg',
      'cast_id': 18,
      'character': 'Agustín Madrigal (voice)',
      'credit_id': '60e6fe4acd2046004668d5a7',
      'order': 3,
    },
    {
      'adult': false,
      'gender': 1,
      'id': 1010157,
      'known_for_department': 'Acting',
      'name': 'Diane Guerrero',
      'original_name': 'Diane Guerrero',
      'popularity': 5.249,
      'profile_path': '/5LXvRYdxKLWROAD2KDSXzRNNSdE.jpg',
      'cast_id': 20,
      'character': 'Isabela Madrigal (voice)',
      'credit_id': '60e6ff1a28723c002d5365a3',
      'order': 4,
    },
    {
      'adult': false,
      'gender': 1,
      'id': 2489411,
      'known_for_department': 'Acting',
      'name': 'Jessica Darrow',
      'original_name': 'Jessica Darrow',
      'popularity': 0.92,
      'profile_path': '/rJojc1ZUasEnUxSk7lL42RxyQbP.jpg',
      'cast_id': 23,
      'character': 'Luisa Madrigal (voice)',
      'credit_id': '60e6ff384ca676005e1a6d00',
      'order': 5,
    },
    {
      'adult': false,
      'gender': 1,
      'id': 120557,
      'known_for_department': 'Acting',
      'name': 'Angie Cepeda',
      'original_name': 'Angie Cepeda',
      'popularity': 5.538,
      'profile_path': '/57OE4acJypRTey5OQlQjkpv14pQ.jpg',
      'cast_id': 22,
      'character': 'Julieta Madrigal  (voice)',
      'credit_id': '60e6ff2db7d3520046d9085e',
      'order': 6,
    },
    {
      'adult': false,
      'gender': 1,
      'id': 3150233,
      'known_for_department': 'Acting',
      'name': 'Adassa',
      'original_name': 'Adassa',
      'popularity': 0.6,
      'profile_path': '/bQPhInWW1B81kpSpHGuJBCmSVV3.jpg',
      'cast_id': 19,
      'character': 'Dolores Madrigal (voice)',
      'credit_id': '60e6feffb76cbb002d46a26e',
      'order': 7,
    },
    {
      'adult': false,
      'gender': 2,
      'id': 3150235,
      'known_for_department': 'Acting',
      'name': 'Mauro Castillo',
      'original_name': 'Mauro Castillo',
      'popularity': 0.6,
      'profile_path': '/qGNGBRyy9LumdFw32M7elnHP4IJ.jpg',
      'cast_id': 21,
      'character': 'Félix Madrigal (voice)',
      'credit_id': '60e6ff2483ee670046fb8c61',
      'order': 8,
    },
    {
      'adult': false,
      'gender': 2,
      'id': 1761634,
      'known_for_department': 'Acting',
      'name': 'Rhenzy Feliz',
      'original_name': 'Rhenzy Feliz',
      'popularity': 2.346,
      'profile_path': '/9YPe3GgyWjG4D0GnTx9KzfvnO0L.jpg',
      'cast_id': 24,
      'character': 'Camilo Madrigal (voice)',
      'credit_id': '60e6ff42b7d3520046d90886',
      'order': 9,
    },
    {
      'adult': false,
      'gender': 1,
      'id': 1489764,
      'known_for_department': 'Acting',
      'name': 'Carolina Gaitán',
      'original_name': 'Carolina Gaitán',
      'popularity': 4.923,
      'profile_path': '/lnmKScZo3GntDNIjaXOkuDeOA90.jpg',
      'cast_id': 25,
      'character': 'Pepa Madrigal (voice)',
      'credit_id': '60e6ff4c4d6791002d8a3ca0',
      'order': 10,
    },
    {
      'adult': false,
      'gender': 2,
      'id': 1969888,
      'known_for_department': 'Acting',
      'name': 'Ravi Cabot-Conyers',
      'original_name': 'Ravi Cabot-Conyers',
      'popularity': 0.84,
      'profile_path': '/m1Uf7UXaet57QUe5IaeCbl3TdHD.jpg',
      'cast_id': 27,
      'character': 'Antonio Madrigal (voice)',
      'credit_id': '613b920dd7107e0062ae27d9',
      'order': 11,
    },
    {
      'adult': false,
      'gender': 2,
      'id': 2362790,
      'known_for_department': 'Acting',
      'name': 'Maluma',
      'original_name': 'Maluma',
      'popularity': 0.84,
      'profile_path': '/2G5nw9Kbhe2UeC9P8ShPWroPCsD.jpg',
      'cast_id': 36,
      'character': 'Mariano (voice)',
      'credit_id': '6196c743bc2cb30090cabc62',
      'order': 12,
    },
    {
      'adult': false,
      'gender': 2,
      'id': 21088,
      'known_for_department': 'Acting',
      'name': 'Alan Tudyk',
      'original_name': 'Alan Tudyk',
      'popularity': 5.798,
      'profile_path': '/jUuUbPuMGonFT5E2pcs4alfqaCN.jpg',
      'cast_id': 37,
      'character': 'Pico (voice)',
      'credit_id': '6196c7531feac10023a52bb9',
      'order': 13,
    },
    {
      'adult': false,
      'gender': 1,
      'id': 53937,
      'known_for_department': 'Acting',
      'name': 'Olga Merediz',
      'original_name': 'Olga Merediz',
      'popularity': 0.952,
      'profile_path': '/AoU4DdgK26xQN4qZTuzgJpVxhos.jpg',
      'cast_id': 39,
      'character': 'Abuela Alma Madrigal (singing voice)',
      'credit_id': '61a96ef47ecd28004622e203',
      'order': 14,
    },
  ],
  'crew': [
    {
      'adult': false,
      'gender': 0,
      'id': 8159,
      'known_for_department': 'Sound',
      'name': 'Shannon Mills',
      'original_name': 'Shannon Mills',
      'popularity': 1.008,
      'profile_path': null,
      'credit_id': '618c6fc91cfe3a0042296490',
      'department': 'Sound',
      'job': 'Supervising Sound Editor',
    },
    {
      'adult': false,
      'gender': 2,
      'id': 69798,
      'known_for_department': 'Production',
      'name': 'Clark Spencer',
      'original_name': 'Clark Spencer',
      'popularity': 0.6,
      'profile_path': null,
      'credit_id': '5e321f4d813cb6001248c5ed',
      'department': 'Production',
      'job': 'Producer',
    },
    {
      'adult': false,
      'gender': 2,
      'id': 76595,
      'known_for_department': 'Acting',
      'name': 'Byron Howard',
      'original_name': 'Byron Howard',
      'popularity': 2.26,
      'profile_path': '/ePJXkxrD44nM0VB7Xx9Q4ityzfT.jpg',
      'credit_id': '5c93afebc3a36861b0e40d95',
      'department': 'Directing',
      'job': 'Director',
    },
    {
      'adult': false,
      'gender': 0,
      'id': 112609,
      'known_for_department': 'Sound',
      'name': 'David E. Fluhr',
      'original_name': 'David E. Fluhr',
      'popularity': 1.646,
      'profile_path': null,
      'credit_id': '6089f25c1d1bf400586bc527',
      'department': 'Sound',
      'job': 'Sound Re-Recording Mixer',
    },
    {
      'adult': false,
      'gender': 1,
      'id': 137198,
      'known_for_department': 'Production',
      'name': 'Jamie Sparer Roberts',
      'original_name': 'Jamie Sparer Roberts',
      'popularity': 0.652,
      'profile_path': '/qAXxHH7GIXTnySqB0GUi7hLxW89.jpg',
      'credit_id': '6089f1e0a6e2d200583293d7',
      'department': 'Production',
      'job': 'Casting',
    },
    {
      'adult': false,
      'gender': 1,
      'id': 1108963,
      'known_for_department': 'Sound',
      'name': 'Germaine Franco',
      'original_name': 'Germaine Franco',
      'popularity': 0.6,
      'profile_path': null,
      'credit_id': '61a0f69ac5c1ef002e8c84f7',
      'department': 'Sound',
      'job': 'Original Music Composer',
    },
    {
      'adult': false,
      'gender': 1,
      'id': 1120694,
      'known_for_department': 'Writing',
      'name': 'Jennifer Lee',
      'original_name': 'Jennifer Lee',
      'popularity': 3.172,
      'profile_path': '/pjZY7hdneR7FLrAT0mXNR6RrPfy.jpg',
      'credit_id': '6089f1c366469a005a385517',
      'department': 'Production',
      'job': 'Executive Producer',
    },
    {
      'adult': false,
      'gender': 2,
      'id': 1179651,
      'known_for_department': 'Acting',
      'name': 'Lin-Manuel Miranda',
      'original_name': 'Lin-Manuel Miranda',
      'popularity': 3.124,
      'profile_path': '/r0wFwPa041pZ1QM66yJWuQXCkqx.jpg',
      'credit_id': '61a99af11f3319002b121bed',
      'department': 'Sound',
      'job': 'Original Music Composer',
    },
    {
      'adult': false,
      'gender': 2,
      'id': 1179651,
      'known_for_department': 'Acting',
      'name': 'Lin-Manuel Miranda',
      'original_name': 'Lin-Manuel Miranda',
      'popularity': 3.124,
      'profile_path': '/r0wFwPa041pZ1QM66yJWuQXCkqx.jpg',
      'credit_id': '60e7402fb76cbb0073e7fb33',
      'department': 'Sound',
      'job': 'Songs',
    },
    {
      'adult': false,
      'gender': 0,
      'id': 1191111,
      'known_for_department': 'Sound',
      'name': 'Justin Doyle',
      'original_name': 'Justin Doyle',
      'popularity': 0.6,
      'profile_path': null,
      'credit_id': '618c6faa0f0da5002aef88a1',
      'department': 'Sound',
      'job': 'Sound Effects Editor',
    },
    {
      'adult': false,
      'gender': 2,
      'id': 1318201,
      'known_for_department': 'Writing',
      'name': 'Jared Bush',
      'original_name': 'Jared Bush',
      'popularity': 1.406,
      'profile_path': '/2gIwj1cnqZIKWaFg0ihmZnuZypR.jpg',
      'credit_id': '5e321f7d326c1900181ee4a2',
      'department': 'Writing',
      'job': 'Screenplay',
    },
    {
      'adult': false,
      'gender': 2,
      'id': 1318201,
      'known_for_department': 'Writing',
      'name': 'Jared Bush',
      'original_name': 'Jared Bush',
      'popularity': 1.406,
      'profile_path': '/2gIwj1cnqZIKWaFg0ihmZnuZypR.jpg',
      'credit_id': '5e321f35326c1900161ee019',
      'department': 'Directing',
      'job': 'Director',
    },
    {
      'adult': false,
      'gender': 0,
      'id': 1397844,
      'known_for_department': 'Sound',
      'name': 'Gabriel Guy',
      'original_name': 'Gabriel Guy',
      'popularity': 0.6,
      'profile_path': null,
      'credit_id': '6089f2687b7b4d003f86869e',
      'department': 'Sound',
      'job': 'Sound Re-Recording Mixer',
    },
    {
      'adult': false,
      'gender': 0,
      'id': 1406872,
      'known_for_department': 'Sound',
      'name': 'Nia Hansen',
      'original_name': 'Nia Hansen',
      'popularity': 1.115,
      'profile_path': null,
      'credit_id': '618c6fb76e0d7200291a6936',
      'department': 'Sound',
      'job': 'Sound Designer',
    },
    {
      'adult': false,
      'gender': 2,
      'id': 1669139,
      'known_for_department': 'Sound',
      'name': 'Samson Neslund',
      'original_name': 'Samson Neslund',
      'popularity': 0.621,
      'profile_path': null,
      'credit_id': '615ce56466a7c3004234e9b9',
      'department': 'Sound',
      'job': 'Sound Effects Editor',
    },
    {
      'adult': false,
      'gender': 0,
      'id': 1710601,
      'known_for_department': 'Writing',
      'name': 'Charise Castro Smith',
      'original_name': 'Charise Castro Smith',
      'popularity': 0.6,
      'profile_path': null,
      'credit_id': '5e321f93c56d2d0013514123',
      'department': 'Writing',
      'job': 'Screenplay',
    },
    {
      'adult': false,
      'gender': 0,
      'id': 1710601,
      'known_for_department': 'Writing',
      'name': 'Charise Castro Smith',
      'original_name': 'Charise Castro Smith',
      'popularity': 0.6,
      'profile_path': null,
      'credit_id': '6089f166f8aee8002901c2c3',
      'department': 'Directing',
      'job': 'Co-Director',
    },
    {
      'adult': false,
      'gender': 0,
      'id': 1994824,
      'known_for_department': 'Lighting',
      'name': 'Nathan Curtis',
      'original_name': 'Nathan Curtis',
      'popularity': 0.618,
      'profile_path': null,
      'credit_id': '6089f1b5f8aee800578bd51e',
      'department': 'Production',
      'job': 'Associate Producer',
    },
    {
      'adult': false,
      'gender': 0,
      'id': 1999218,
      'known_for_department': 'Production',
      'name': 'Yvett Merino Flores',
      'original_name': 'Yvett Merino Flores',
      'popularity': 0.6,
      'profile_path': null,
      'credit_id': '5e321f66813cb6001448acab',
      'department': 'Production',
      'job': 'Producer',
    },
    {
      'adult': false,
      'gender': 0,
      'id': 2005764,
      'known_for_department': 'Visual Effects',
      'name': 'Mehrdad Isvandi',
      'original_name': 'Mehrdad Isvandi',
      'popularity': 0.6,
      'profile_path': null,
      'credit_id': '6089f1ec67e0f7006fb25eb2',
      'department': 'Art',
      'job': 'Art Direction',
    },
  ],
};
