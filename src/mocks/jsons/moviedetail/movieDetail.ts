/* eslint-disable quotes */
export const mockMovieDetail = {
  'adult': false,
  'backdrop_path': '/g2djzUqA6mFplzC03gDk0WSyg99.jpg',
  'belongs_to_collection': null,
  'budget': 0,
  'genres': [
    {
      'id': 12,
      'name': 'Adventure',
    },
    {
      'id': 16,
      'name': 'Animation',
    },
    {
      'id': 35,
      'name': 'Comedy',
    },
    {
      'id': 10751,
      'name': 'Family',
    },
    {
      'id': 14,
      'name': 'Fantasy',
    },
    {
      'id': 10402,
      'name': 'Music',
    },
  ],
  'homepage': 'https://movies.disney.com/encanto',
  'id': 568124,
  'imdb_id': 'tt2953050',
  'original_language': 'en',
  'original_title': 'Encanto',
  'overview':
    "The tale of an extraordinary family, the Madrigals, who live hidden in the mountains of Colombia, in a magical house, in a vibrant town, in a wondrous, charmed place called an Encanto. The magic of the Encanto has blessed every child in the family with a unique gift from super strength to the power to heal—every child except one, Mirabel. But when she discovers that the magic surrounding the Encanto is in danger, Mirabel decides that she, the only ordinary Madrigal, might just be her exceptional family's last hope.",
  'popularity': 2561.333,
  'poster_path': '/4j0PNHkMr5ax3IA8tjtxcmPU3QT.jpg',
  'production_companies': [
    {
      'id': 6125,
      'logo_path': '/tVPmo07IHhBs4HuilrcV0yujsZ9.png',
      'name': 'Walt Disney Animation Studios',
      'origin_country': 'US',
    },
    {
      'id': 2,
      'logo_path': '/wdrCwmRnLFJhEoH8GSfymY85KHT.png',
      'name': 'Walt Disney Pictures',
      'origin_country': 'US',
    },
  ],
  'production_countries': [
    {
      'iso_3166_1': 'US',
      'name': 'United States of America',
    },
  ],
  'release_date': '2021-11-24',
  'revenue': 73000000,
  'runtime': 109,
  'spoken_languages': [
    {
      'english_name': 'English',
      'iso_639_1': 'en',
      'name': 'English',
    },
    {
      'english_name': 'Spanish',
      'iso_639_1': 'es',
      'name': 'Español',
    },
  ],
  'status': 'Released',
  'tagline': "There's a little magic in all of us ...almost all of us.",
  'title': 'Encantada',
  'video': false,
  'vote_average': 7.5,
  'vote_count': 178,
};
