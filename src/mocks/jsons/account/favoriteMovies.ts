export const mockFavoriteMovies = {
  'page': 1,
  'results': [
    {
      'poster_path': '/rjkmN1dniUHVYAtwuV3Tji7FsDO.jpg',
      'video': false,
      'vote_average': 7.2,
      'overview':
        'After finding a host body in investigative reporter Eddie Brock, the alien symbiote must face a new enemy, Carnage, the alter ego of serial killer Cletus Kasady.',
      'release_date': '2021-09-30',
      'vote_count': 3947,
      'adult': false,
      'backdrop_path': '/lNyLSOKMMeUPr1RsL4KcRuIXwHt.jpg',
      'title': 'Venom: Let There Be Carnage',
      'genre_ids': [878, 28, 12],
      'id': 580489,
      'original_language': 'en',
      'original_title': 'Venom: Let There Be Carnage',
      'popularity': 11473.714,
    },
    {
      'video': false,
      'vote_average': 7.6,
      'overview':
        'Natasha Romanoff, also known as Black Widow, confronts the darker parts of her ledger when a dangerous conspiracy with ties to her past arises. Pursued by a force that will stop at nothing to bring her down, Natasha must deal with her history as a spy and the broken relationships left in her wake long before she became an Avenger.',
      'release_date': '2021-07-07',
      'vote_count': 6165,
      'adult': false,
      'backdrop_path': '/keIxh0wPr2Ymj0Btjh4gW7JJ89e.jpg',
      'id': 497698,
      'genre_ids': [28, 12, 878],
      'title': 'Black Widow',
      'original_language': 'en',
      'original_title': 'Black Widow',
      'poster_path': '/qAZ0pzat24kLdO3o8ejmbLxyOac.jpg',
      'popularity': 498.634,
    },
    {
      'adult': false,
      'backdrop_path': '/tiIpajUBpLMNWMEzpjRBxo0jCbD.jpg',
      'genre_ids': [18, 53, 12, 27],
      'original_language': 'en',
      'original_title': 'Cargo',
      'poster_path': '/cdPSUck4tBRvRu6DFk6XciDrssn.jpg',
      'video': false,
      'vote_average': 6.4,
      'vote_count': 1327,
      'overview':
        'After being infected in the wake of a violent pandemic and with only 48 hours to live, a father struggles to find a new home for his baby daughter.',
      'release_date': '2017-10-06',
      'title': 'Cargo',
      'id': 425972,
      'popularity': 15.427,
    },
  ],
  'total_pages': 1,
  'total_results': 3,
};
