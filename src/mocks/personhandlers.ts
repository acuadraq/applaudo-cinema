import { rest } from 'msw';
import { mockPersonCredits } from './jsons/person/personCredits';
import { mockPersonInfo } from './jsons/person/personInfo';

const baseURL = 'https://api.themoviedb.org/3';

export const personhandlers = [
  rest.get(`${baseURL}/person/2524`, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockPersonInfo));
  }),

  rest.get(`${baseURL}/person/2524/combined_credits`, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockPersonCredits));
  }),

  rest.post(`${baseURL}/authentication/session/new`, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json({ success: true, session_id: 'asdaas123asdasd' }));
  }),

  rest.get(`${baseURL}/authentication/token/new`, (req, res, ctx) => {
    return res(
      ctx.status(200),
      ctx.json({ success: true, expires_at: '2022-08-26 17:04:39 UTC', request_token: 'asdasda' }),
    );
  }),

  rest.get(`${baseURL}/account`, (req, res, ctx) => {
    return res(
      ctx.status(200),
      ctx.json({
        'avatar': {
          'gravatar': {
            'hash': '65a5ec8debf5e85bf280391c1d523b0f',
          },
          'tmdb': {
            'avatar_path': '/5S9dcNdSWzwcrrTrhR90k6rW45W.jpg',
          },
        },
        'id': 11471181,
        'iso_639_1': 'en',
        'iso_3166_1': 'NI',
        'name': 'Alex Cuadra',
        'include_adult': false,
        'username': 'AlexC',
      }),
    );
  }),
];
