import { movieHandlers } from './movieHandlers';
import { tvhandlers } from './tvhandlers';
import { personhandlers } from './personhandlers';

const handlers = [...movieHandlers, ...tvhandlers, ...personhandlers];

export default handlers;
