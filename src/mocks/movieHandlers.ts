import { rest } from 'msw';
import { mockFavoriteMovies } from './jsons/account/favoriteMovies';
import { mockMovieCast } from './jsons/moviedetail/movieCast';
import { mockMovieDetail } from './jsons/moviedetail/movieDetail';
import { mockMovieImages } from './jsons/moviedetail/movieImages';
import { mockMovieReviews } from './jsons/moviedetail/movieRevies';
import { mockMovieSimilar } from './jsons/moviedetail/movieSimilar';
import { mockCertifications } from './jsons/movies/certifications';
import { mockMoviesList } from './jsons/movies/discoverMovies';
import { mockGenresMovies } from './jsons/movies/geenresMovie';
import { mockMovieFilter } from './jsons/movies/moviesFilter';
import { mockMoviesNowPlaying } from './jsons/movies/nowPlaying';
import { mockMoviesPopular } from './jsons/movies/popular';
import { mockMovieTopRated } from './jsons/movies/top_rated';
import { mockSearch } from './jsons/search/search';

const baseURL = 'https://api.themoviedb.org/3';

export const movieHandlers = [
  rest.get(`${baseURL}/movie/now_playing`, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockMoviesNowPlaying));
  }),
  rest.get(`${baseURL}/movie/popular`, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockMoviesPopular));
  }),
  rest.get(`${baseURL}/movie/top_rated`, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockMovieTopRated));
  }),
  rest.get(`${baseURL}/discover/movie`, (req, res, ctx) => {
    const certificationParam = req.url.searchParams.get('certification');
    if (certificationParam) return res(ctx.status(200), ctx.json(mockMovieFilter));
    const genresParam = req.url.searchParams.get('with_genres');
    if (genresParam) return res(ctx.status(200), ctx.json(mockMovieFilter));
    return res(ctx.status(200), ctx.json(mockMoviesList));
  }),

  rest.get(`${baseURL}/genre/movie/list`, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockGenresMovies));
  }),

  rest.get(`${baseURL}/certification/movie/list`, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockCertifications));
  }),

  rest.get(`${baseURL}/search/multi`, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockSearch));
  }),

  rest.get(`${baseURL}/movie/568124`, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockMovieDetail));
  }),

  rest.get(`${baseURL}/movie/568124/credits`, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockMovieCast));
  }),

  rest.get(`${baseURL}/movie/568124/images`, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockMovieImages));
  }),

  rest.get(`${baseURL}/movie/568124/similar`, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockMovieSimilar));
  }),

  rest.get(`${baseURL}/movie/568124/reviews`, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockMovieReviews));
  }),

  rest.get(`${baseURL}/movie/568124/account_states`, (req, res, ctx) => {
    return res(
      ctx.status(200),
      ctx.json({ id: 568124, favorite: false, rated: false, watchlist: false }),
    );
  }),

  rest.get(`${baseURL}/account/11471181/favorite/movies`, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockFavoriteMovies));
  }),

  rest.get(`${baseURL}/account/undefined/favorite/movies`, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockFavoriteMovies));
  }),

  rest.post(`${baseURL}/account/11471181/favorite`, (req, res, ctx) => {
    return res(
      ctx.status(200),
      ctx.json({ status_code: 200, status_message: 'you did it', success: true }),
    );
  }),
];
