import { rest } from 'msw';
import { mockFavoriteTV } from './jsons/account/favoriteTV';
import { mockEpisode } from './jsons/tvdetail/episodes';
import { mockTvCredits } from './jsons/tvdetail/tvcredits';
import { mockTvCredits2 } from './jsons/tvdetail/tvcredits2';
import { mockTvDetail } from './jsons/tvdetail/tvdetail';
import { mockTVdetail2 } from './jsons/tvdetail/tvdetail2';
import { mockTvImages } from './jsons/tvdetail/tvimages';
import { mockTvReviews } from './jsons/tvdetail/tvreviews';
import { mockTvSimilar } from './jsons/tvdetail/tvsimilar';
import { mockTvList } from './jsons/tvshows/discovertv';
import { mockGenresTV } from './jsons/tvshows/genrestv';
import { mockTvOnAir } from './jsons/tvshows/onAir';
import { mockTVFilter } from './jsons/tvshows/tvfilter';

const baseURL = 'https://api.themoviedb.org/3';

export const tvhandlers = [
  rest.get(`${baseURL}/discover/tv`, (req, res, ctx) => {
    const genresParam = req.url.searchParams.get('with_genres');
    if (genresParam) return res(ctx.status(200), ctx.json(mockTVFilter));
    return res(ctx.status(200), ctx.json(mockTvList));
  }),

  rest.get(`${baseURL}/account/11471181/favorite/tv`, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockFavoriteTV));
  }),

  rest.get(`${baseURL}/genre/tv/list`, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockGenresTV));
  }),

  rest.get(`${baseURL}/tv/on_the_air`, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockTvOnAir));
  }),

  rest.get(`${baseURL}/tv/88329`, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockTvDetail));
  }),

  rest.get(`${baseURL}/tv/106651`, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockTVdetail2));
  }),

  rest.get(`${baseURL}/tv/88329/credits`, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockTvCredits));
  }),

  rest.get(`${baseURL}/tv/106651/credits`, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockTvCredits2));
  }),

  rest.get(`${baseURL}/tv/88329/images`, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockTvImages));
  }),

  rest.get(`${baseURL}/tv/88329/similar`, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockTvSimilar));
  }),

  rest.get(`${baseURL}/tv/88329/reviews`, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockTvReviews));
  }),

  rest.get(`${baseURL}/tv/1416/season/7`, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockEpisode));
  }),

  rest.get(`${baseURL}/tv/88329/account_states`, (req, res, ctx) => {
    return res(
      ctx.status(200),
      ctx.json({ id: 88329, favorite: false, rated: false, watchlist: false }),
    );
  }),

  rest.post(`${baseURL}/account/11471181/favorite`, (req, res, ctx) => {
    return res(
      ctx.status(200),
      ctx.json({ status_code: 200, status_message: 'you did it', success: true }),
    );
  }),
];
