const urlPoster = 'https://image.tmdb.org/t/p/w300';
const urlFull = 'https://image.tmdb.org/t/p/w1920_and_h800_multi_faces';
const url500 = 'https://image.tmdb.org/t/p/w500';
const urlSeason = 'https://image.tmdb.org/t/p/w130_and_h195_bestv2';
const smallPoster = 'https://image.tmdb.org/t/p/w58_and_h87_face';
const profilePath = 'https://image.tmdb.org/t/p/w66_and_h66_face';
const personPic = 'https://image.tmdb.org/t/p/w300_and_h450_bestv2';
const avatar = 'https://image.tmdb.org/t/p/w150_and_h150_face';

export const getPosterImage = (url: string | null | undefined) => `${urlPoster}${url}`;

export const getFullImage = (url: string | null | undefined) => `${urlFull}${url}`;

export const getW500 = (url: string | null) => `${url500}${url}`;

export const getSeasonURL = (url: string | null | undefined) => `${urlSeason}${url}`;

export const getSmallerPoster = (url: string | null | undefined) => `${smallPoster}${url}`;

export const getProfilePic = (url: string | null | undefined) => `${profilePath}${url}`;

export const getPersonProfile = (url: string | null | undefined) => `${personPic}${url}`;

export const getAvatar = (url: string | null | undefined) => `${avatar}${url}`;
