import { fetchApi } from './Fetch';
import { ICertification } from '../interfaces/certification';
import { IGenres } from '../interfaces/genres';
import { IMovies } from '../interfaces/movies';
import IMovie from '../interfaces/movie';
import { ICast } from '../interfaces/credits';
import { IImages } from '../interfaces/images';
import { IReview } from '../interfaces/reviews';
import { IIsInFavorites } from '../interfaces/login';

export const getNowPlaying = () => fetchApi<IMovies>('/movie/now_playing');

export const getPopular = () => fetchApi<IMovies>('/movie/popular');

export const getDiscovered = (page?: number, query?: string) =>
  fetchApi<IMovies>('/discover/movie', page, `&sort_by=popularity.desc${query ? query : ''}`);

export const getTopRated = () => fetchApi<IMovies>('/movie/top_rated');

export const getGenresMovie = () => fetchApi<IGenres>('/genre/movie/list');

export const getCertificationsMovies = () => fetchApi<ICertification>('/certification/movie/list');

//  For Movie Detail

export const getMovieDetail = (id: number) => fetchApi<IMovie>(`/movie/${id}`);

export const getCast = (id: number) => fetchApi<ICast>(`/movie/${id}/credits`);

export const getImagesMovies = (id: number) => fetchApi<IImages>(`/movie/${id}/images`);

export const getSimilar = (id: number) => fetchApi<IMovies>(`/movie/${id}/similar`);

export const getReviews = (id: number) => fetchApi<IReview>(`/movie/${id}/reviews`);

export const checkIsInFavorites = (id: number, sessionId: string) =>
  fetchApi<IIsInFavorites>(`/movie/${id}/account_states`, undefined, `&session_id=${sessionId}`);
