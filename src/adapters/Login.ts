import {
  IAccount,
  ILogout,
  INewToken,
  IPostFavorite,
  ISession,
  ISessionID,
  ISessionPost,
  ISuccessFavorite,
} from '../interfaces/login';
import { IMovies } from '../interfaces/movies';
import { ITvShows } from '../interfaces/tvshows';
import { fetchApi, postApi } from './Fetch';

export const generateToken = () => fetchApi<INewToken>('/authentication/token/new');

export const postSession = (requestToken: ISessionPost) =>
  postApi<ISession>('/authentication/session/new', 'POST', requestToken);

export const getAccountInfo = (sessionId: string) =>
  fetchApi<IAccount>('/account', undefined, `&session_id=${sessionId}`);

export const favoriteMovies = (id: number, sessionId: string) =>
  fetchApi<IMovies>(`/account/${id}/favorite/movies`, undefined, `&session_id=${sessionId}`);

export const favoriteTv = (id: number, sessionId: string) =>
  fetchApi<ITvShows>(`/account/${id}/favorite/tv`, undefined, `&session_id=${sessionId}`);

export const postFavorites = (
  accountId: number,
  objectFavorite: IPostFavorite,
  sessionId: string,
) =>
  postApi<ISuccessFavorite>(
    `/account/${accountId}/favorite`,
    'POST',
    objectFavorite,
    `&session_id=${sessionId}`,
  );

export const deleteSession = (sessionId: ISessionID) =>
  postApi<ILogout>('/authentication/session', 'DELETE', sessionId);
