import axios, { AxiosResponse, Method } from 'axios';
import { IPostFavorite, ISessionID, ISessionPost } from '../interfaces/login';
import { ICreditsPerson, IPerson } from '../interfaces/person';
import { ISearch } from '../interfaces/search';

const baseURL = 'https://api.themoviedb.org/3';
const { REACT_APP_API_KEY } = process.env;

export const fetchApi = async <T>(extension: string, page?: number, query?: string): Promise<T> => {
  const request: AxiosResponse = await axios(
    `${baseURL}${extension}?api_key=${REACT_APP_API_KEY}${page ? `&page=${page}` : ''}${
      query ? query : ''
    }`,
  );
  return request.data;
};

export const postApi = async <T>(
  extension: string,
  type: Method,
  object: ISessionPost | IPostFavorite | ISessionID,
  query?: string,
): Promise<T> => {
  const request: AxiosResponse = await axios({
    method: type,
    url: `${baseURL}${extension}?api_key=${REACT_APP_API_KEY}${query ? query : ''}`,
    data: object,
    headers: {
      'Accept': 'application/json',
      'Content-type': 'application/json',
    },
  });
  return request.data;
};

export const searchQuery = (page?: number, query?: string) =>
  fetchApi<ISearch>('/search/multi', page, query);

export const getPersonInfo = (id: number) => fetchApi<IPerson>(`/person/${id}`);

export const getPersonCredits = (id: number) =>
  fetchApi<ICreditsPerson>(`/person/${id}/combined_credits`);
