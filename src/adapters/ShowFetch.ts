import { fetchApi } from './Fetch';
import { ITvShows } from '../interfaces/tvshows';
import { IGenres } from '../interfaces/genres';
import { IShow } from '../interfaces/tv';
import { ICast } from '../interfaces/credits';
import { IImages } from '../interfaces/images';
import { IReview } from '../interfaces/reviews';
import { ISeason } from '../interfaces/season';
import { IIsInFavorites } from '../interfaces/login';

export const getOnAirShows = () => fetchApi<ITvShows>('/tv/on_the_air');

export const getShows = (page?: number, query?: string) =>
  fetchApi<ITvShows>('/discover/tv', page, `&sort_by=popularity.desc${query ? query : ''}`);

export const getGenreShows = () => fetchApi<IGenres>('/genre/tv/list');

//  For TV Detail

export const getTvDetail = (id: number) => fetchApi<IShow>(`/tv/${id}`);

export const getTvCast = (id: number) => fetchApi<ICast>(`/tv/${id}/credits`);

export const getTvImages = (id: number) => fetchApi<IImages>(`/tv/${id}/images`);

export const getTvReviews = (id: number) => fetchApi<IReview>(`/tv/${id}/reviews`);

export const getSimilarTV = (id: number) => fetchApi<ITvShows>(`/tv/${id}/similar`);

export const getSeason = (id: number, seasonNumber: number) =>
  fetchApi<ISeason>(`/tv/${id}/season/${seasonNumber}`);

export const checkTVInFavorites = (id: number, sessionId: string) =>
  fetchApi<IIsInFavorites>(`/tv/${id}/account_states`, undefined, `&session_id=${sessionId}`);
