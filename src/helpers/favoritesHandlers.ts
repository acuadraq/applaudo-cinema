import { postFavorites } from '../adapters/Login';
import { IPostFavorite, ISuccessFavorite } from '../interfaces/login';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.min.css';

const addOrRemove = async (
  submitObject: IPostFavorite,
  accountId: number,
  sessionId: string,
  type: string,
) => {
  try {
    const submit: ISuccessFavorite = await postFavorites(accountId, submitObject, sessionId);
    if (!submit.success) {
      toast.error(`There was an error it couldn't been ${type === 'add' ? 'added' : 'removed'}`);
    } else {
      toast.success(`${type === 'add' ? 'Added to' : 'Removed from'} your favorites!`, {
        position: 'top-center',
        autoClose: 3000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: false,
        draggable: false,
        progress: undefined,
      });
    }
    return submit;
  } catch (err) {
    toast.error(`There was an error it couldn't been ${type === 'add' ? 'added' : 'removed'}`);
  }
};

export default addOrRemove;
