module.exports = {
  purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
  darkMode: false,
  theme: {
    fontFamily: {
      poppins: ['Poppins', 'sans-serif'],
    },
    extend: {
      spacing: {
        sliderHeight: '720px',
        cardsheight: '350px',
        cardswidth: '250px',
        seasonwidth: '130px',
        seasonheight: '195px',
        episodewidth: '300px',
        episodeheight: '150px',
      },
      colors: {
        bgcolor: '#222831',
        cardscolor: '#30475E',
        cardshover: '#364E67',
        palidcolor: '#DDDDDD',
        buttonscolor: '#F05454',
        hovertext: '#C8C2C2',
        bgsecondcolor: '#29303A',
      },
      screens: {
        'phonemini': '400px',
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
