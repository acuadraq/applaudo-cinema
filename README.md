# Final Project

![](public/Screenshot.png)

## Description

This is the project final, is a movie database page, and has all this functionalities:

- Every media displayed is retrieve from an API
- There is a home page where it showes some movies and tv shows
- There is a movie list page, where you can filter by certfications, genres and dates
- There is a movie list page, where you can filter by genres and dates
- You can search for a movie, tv show or a person, on a input field on the top of the page
- You can see the detail of a movie, tv show and person
- On the detail of the tvs and movies you can add it to favorites or remove it, only if you are logged in
- You can log in only if you have an account created from the movie database
- You can see your favorite movies and tvs on your account
- You can log out
- There are test that covered 85% of the code

If you want to tested in real time use this url: https://applaudo-cinema.vercel.app/
